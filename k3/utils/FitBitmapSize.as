package k3.utils
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	public class FitBitmapSize
	{
		public function FitBitmapSize()
		{
		}
		
		public static function maxFit(source:Bitmap, cur_width:Number, cur_height:Number):Bitmap
		{
			
			//如果比目标小就直接用下面的方法
			if (cur_width < source.width) return fit(source, cur_width, cur_height, 0, 0);
			
			var bm:Bitmap=new Bitmap(source.bitmapData.clone());
			
			var maxRat:Number;
			if (source.width >= source.height)
			{
				maxRat = cur_width / source.width;//获取宽度最大倍数
				for (var i:Number = maxRat; i > 1; i -= 0.01)
				{
					if ((source.height *=i) <= cur_height)
					{
						maxRat = i;
						break;
					}
				}
				
			}
			else
			{
				maxRat = cur_height / source.height;//获取宽度最大倍数
				for (var o:Number = maxRat; o > 1; o -= 0.01)
				{
					if ((source.height *= o)<= cur_height)
					{
						maxRat = o;
						break;
					}
				}
			}
			
			bm.width *= maxRat;
			bm.height *= maxRat;
			source.bitmapData.dispose();
			return bm;
		}
		
		
		public static function normalFit(source:Bitmap, cur_width:Number, cur_height:Number):Bitmap
		{
			var rat:Number = source.width / source.height;
			if (source.width >= source.height)
			{
				if (source.width > cur_width)
				{
					source.width = cur_width;
					source.height = cur_width / rat;
				}
				if (source.height > cur_height)
				{
					source.height = cur_height;
					source.width = cur_height * rat;
				}
			}
			else
			{
				if (source.height > cur_height)
				{
					source.height = cur_height;
					source.width = cur_height * rat;
				}
				if (source.width > cur_width)
				{
					source.width = cur_width;
					source.height = cur_width / rat;
				}
			}
			return source;
		}
		
		public static function fitOut(source:Bitmap, cur_width:Number, cur_height:Number):Bitmap
		{
			var rat:Number = source.width / source.height;
			if (source.width >= source.height)
			{
				if (source.width > cur_width)
				{
					source.width = cur_width;
					source.height = cur_width / rat;
				}
				if (source.height < cur_height)
				{
					source.height = cur_height;
					source.width = cur_height * rat;
				}
			}
			else
			{
				if (source.height > cur_height)
				{
					source.height = cur_height;
					source.width = cur_height * rat;
				}
				
				if (source.width < cur_width)
				{
					source.width = cur_width;
					source.height = cur_width / rat;
				}
				
			}
			return source;
		}
		
		public static function fit(source:Bitmap, cur_width:Number, cur_height:Number, fillColor:Number,fillAlpha:Number=1):Bitmap
		{
			var sp:Sprite=createSprite(cur_width, cur_height, fillColor,fillAlpha);
			var rat:Number = source.width / source.height;
			if (source.width >= source.height)
			{
				if (source.width > cur_width)
				{
					source.width = cur_width;
					source.height = cur_width / rat;
				}
				if (source.height > cur_height)
				{
					source.height = cur_height;
					source.width = cur_height * rat;
				}
			}
			else
			{
				if (source.height > cur_height)
				{
					source.height = cur_height;
					source.width = cur_height * rat;
				}
				if (source.width > cur_width)
				{
					source.width = cur_width;
					source.height = cur_width / rat;
				}
			}
			
			source.x = cur_width / 2 - source.width / 2;
			source.y = cur_height / 2 - source.height / 2;
			sp.addChild(source);
			var bitmap:Bitmap = BitMapMethod.setBitmap(sp);
			
			return bitmap;
		}
		
		private static function createSprite(cur_width:Number, cur_height:Number, fillColor:Number, fillAlpha:Number = 1):Sprite
		{
			var sp:Sprite = new Sprite();
			sp.graphics.beginFill(fillColor, fillAlpha);
			sp.graphics.drawRect(0, 0, cur_width, cur_height);
			sp.graphics.endFill();
			return sp;
		}
	}
}
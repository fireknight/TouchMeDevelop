package k3.utils
{
	import flash.utils.ByteArray;
	import flash.utils.Endian;
	public class StringTool
	{
		public function StringTool()
		{
		}
		
		/**
		 * 判断是否是中文
		 */
		public static function checkChinese(strSrouce:String):Boolean
		{
			var pat:RegExp = new RegExp(/[\u4e00-\u9fa5]/);
			return pat.test(strSrouce);
		}
		
		
		/**
		 * Repalce all by split and join;
		 */
		public static function replaceAllBySplit(strSource:String, strReplaceFrom:String, strRepalceTo:String):String
		{
			return strSource == null ? null : strSource.split(strReplaceFrom).join(strRepalceTo);
		}
		
		
		public static function findByString(strSource:String, strReplaceFrom:String):Boolean
		{
			var bo:Boolean = false;
			if (strSource.indexOf(strReplaceFrom) != -1)
			{
				bo = true;
			}
			return bo;
		}
		
		/**
		 * Replace all by RegEx;
		 */
		public static function replaceAllByRegex(strSource:String, strReplaceFrom:String, strRepalceTo:String):String
		{
			return strSource == null ? null : strSource.replace(new RegExp(strReplaceFrom, 'g'), strRepalceTo);
		}
		
		public static function haveNumber(strSource:String):Boolean
		{
			var rep:RegExp =/\b\d/;
			return rep.test(strSource);
		}
		
		//判断是否都是数字
		public static function isAllNumber(strSource:String):Boolean
		{
			var pattern:RegExp=/\D/g;
			var haveNoNumber:Boolean=pattern.test(strSource);
			//如果包含有非数字就为true,否则为false,所以这里取反
			return !haveNoNumber;
		}
		
		public static function findByRegex(strSource:String, strReplaceFrom:String):Boolean
		{
			var rep:RegExp = new RegExp(strReplaceFrom, 'g');
			return rep.test(strSource);
		}
		
		/**
		 *  删除指定字符串的开头和结尾的所有的空白
		 * 
		 */
		public static function trim(str:String):String
		{
			var pattern:RegExp=/^(\s)|\s$/g;
			return str.replace(pattern,"");
		}
		
		/**
		 * 
		 * @param	value		待处理字符串
		 * @param	delimiter	参数分隔符分隔字符串中的每个数组元素的字符串。如果没有""
		 * @return	返回的更新字符串
		 */
		public static function trimArrayElements(value:String, delimiter:String):String
		{
			if (value != "" && value != null)
			{
				var items:Array = value.split(delimiter);
				
				var len:int = items.length;
				for (var i:int = 0; i < len; i++)
				{
					items[i] = trim(items[i]);
				}
				
				if (len > 0)
				{
					value = items.join(delimiter);
				}
			}
			
			return value;
		}
		
		//判断是否是空白
		public static function isWhitespace(character:String):Boolean
		{
			var rep:RegExp =/\s/g;
			return rep.test(character);
		}
		
		/**
		 *  去除文本换行
		 * @param	s
		 * @return
		 */
		public static function clearRow(s:String):String
		{
			var rep:RegExp =/[\t\n\r\f]/g;
			return s.replace(rep, "");
		}
		
		
		/**
		 * 判断是否是IP地址
		 * @param value
		 * @return 
		 * 
		 */		
		public static function isIpAddress(value:String):Boolean
		{
			var arr:Array=value.split(".");
			var num:Number;
			
			if(arr.length!=4)return false;
			//不是4段就不是IPV4
			for(var i:uint=0;i<arr.length;i++)
			{
				
				if(!isAllNumber(arr[i]))
				{
					//如果不是数字就返回
					return false;
				}
				num=Number(arr[i]);
				if(num<0 || num>255)
				{
					return false;
				}
				
			}
			return true;
		}
		
		public static function StrToByteArray( strValue:String, uLen:uint = 0,endia:String="little" ):ByteArray  
		{  
			var byAaryR:ByteArray = new ByteArray();  
			if(endia=="little")
			{
				byAaryR.endian = Endian.LITTLE_ENDIAN;  
			}
			else
			{
				byAaryR.endian = Endian.BIG_ENDIAN;  
			}
			for ( var i:int = 0; i < strValue.length; ++i )  
			{  
				byAaryR.writeShort(strValue.charCodeAt(i));  
			}  
			for ( i = 0; i < (uLen - strValue.length); ++i )  
			{  
				byAaryR.writeShort(0);  
			}  
			return byAaryR;  
		}  
		
		
		//ByteArray to unicode string  
		public static function byArrayToString( byArray:ByteArray ):String  
		{  
			byArray.position = 0;  
			var strReturn:String = new String();  
			for(var i:int = 0; i < byArray.length / 2; ++i)  
			{  
				var strRep:String = byArray.readMultiByte(2, "unicode");  
				strReturn = replaceAt(strReturn, strRep, i, i);  
			}  
			return strReturn;  
		}  
		
		public static function replaceAt(char:String, value:String, beginIndex:int, endIndex:int):String   
		{  
			beginIndex = Math.max(beginIndex, 0);  
			endIndex = Math.min(endIndex, char.length);  
			var firstPart:String = char.substr(0, beginIndex);  
			var secondPart:String = char.substr(endIndex, char.length);  
			return (firstPart + value + secondPart);  
		}  
	}
}
package k3.utils
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.JPEGEncoderOptions;
	import flash.display.PNGEncoderOptions;
	import flash.display.StageQuality;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	
	import k3.event.KEvent;
	public class CopyToImageFile extends EventDispatcher
	{
		private var fs:FileStream = new FileStream(); 
		private var bmpTarget:BitmapData;
		private var quality:uint=80;
		private var rect:Rectangle;
		private var byteArray:ByteArray;
		private var transparent:Boolean;
		public static const SUCCESS:String="imageSaved";
		public static const FAILURE:String="imageSavedFailure";
		public function CopyToImageFile(target:IEventDispatcher=null)
		{
			super(target);
		}
		
		public function saveJpg(target:DisplayObject,rect:Rectangle,quality:uint,savePosition:File,async:Boolean=true):void
		{
			bmpTarget = new BitmapData(target.width,target.height);
			bmpTarget.drawWithQuality(target,new Matrix(),null,null,null,true,StageQuality.BEST);
			
			byteArray = new ByteArray();
			this.rect=rect;
			this.quality=quality;
			try
			{ 
				if(async)
				{
					fs.addEventListener(Event.COMPLETE,handleFileOpenComplete);
					fs.openAsync(savePosition,FileMode.UPDATE); 
				}
				else
				{
					//open file in write mode 
					fs.open(savePosition,FileMode.UPDATE); 
					//write bytes from the byte array 
					fs.writeBytes(bmpTarget.encode(rect, new JPEGEncoderOptions(quality), byteArray)); 
					//close the file 
					fs.close(); 
					this.dispatchEvent(new KEvent(SUCCESS));
				}
				
			} 
			catch (e:Error)
			{ 
				this.dispatchEvent(new KEvent(FAILURE));
				trace(e.message); 
			} 
		}
		
		
		private function handleFileOpenComplete(e:Event):void
		{
			
			fs.removeEventListener(Event.COMPLETE,handleFileOpenComplete);
			fs.writeBytes(bmpTarget.encode(rect, new JPEGEncoderOptions(quality), byteArray)); 
			fs.close(); 
			this.dispatchEvent(new KEvent(SUCCESS));
			
		}
		
		public function savePng(target:DisplayObject,rect:Rectangle,transparent:Boolean,savePosition:File,async:Boolean=true):void
		{
			this.transparent=transparent;
			
			bmpTarget = new BitmapData(target.width,target.height,transparent,0x00000000);
			bmpTarget.drawWithQuality(target,new Matrix(),null,null,null,true,StageQuality.BEST);
			byteArray = new ByteArray();
			this.rect=rect;
			try
			{ 
				if(async)
				{
					fs.addEventListener(Event.COMPLETE,handlePngsaveComplete);
					fs.openAsync(savePosition,FileMode.UPDATE); 
				}
				else
				{
					//open file in write mode 
					fs.open(savePosition,FileMode.UPDATE); 
					//write bytes from the byte array 
					fs.writeBytes(bmpTarget.encode(rect, new PNGEncoderOptions(transparent), byteArray)); 
					//close the file 
					fs.close(); 
					this.dispatchEvent(new KEvent(SUCCESS));
				}
				
			} 
			catch (e:Error)
			{ 
				this.dispatchEvent(new KEvent(FAILURE));
				trace(e.message); 
			} 
		}
		
		private function handlePngsaveComplete(e:Event):void
		{
			
			fs.removeEventListener(Event.COMPLETE,handlePngsaveComplete);
			fs.writeBytes(bmpTarget.encode(rect, new PNGEncoderOptions(transparent), byteArray)); 
			fs.close(); 
			this.dispatchEvent(new  KEvent(SUCCESS));
		}
		

	}
}
package k3.utils
{
	import flash.utils.ByteArray;
	import flash.utils.Endian;
	public class NumberTool
	{
		public function NumberTool()
		{
		}
		
		/**
		 * 
		 * @param	data		需要转换的"字符串"
		 * @param	sourceUnit	原始单位 K/M
		 * @param	targetUnit	转换成单位 K/M
		 * @param	nAfterDot	保留小数点位数
		 * @return	如果错误，返回原data
		 */
		public static function getFileSize(data:String,sourceUnit:String="B",targetUnit:String="K",nAfterDot:int=0):String
		{
			var dataNum:Number = Number(data);
			if (isNaN(dataNum))
			{
				return data;
			}
			if (sourceUnit == "B")
			{
				if (targetUnit == "K")
				{
					return FormatNumber(Number(bToK(data)), nAfterDot).toString();
				}
				else if ( targetUnit == "M")
				{
					return FormatNumber(Number(bToM(data)), nAfterDot).toString();
				}
				else if (targetUnit == "B")
				{
					return FormatNumber(Number(data), nAfterDot).toString();
				}
				return data;
			}
			else if (sourceUnit == "K")
			{
				if (targetUnit == "M")
				{
					return FormatNumber(Number(kToM(data)), nAfterDot).toString();
				}
				else if (targetUnit == "B")
				{
					return FormatNumber(Number(kToB(data)), nAfterDot).toString();
				}
				else if (targetUnit == "K")
				{
					return FormatNumber(Number(data), nAfterDot).toString();
				}
				return data;
			}
			else if (sourceUnit == "M")
			{
				if (targetUnit == "B")
				{
					return FormatNumber(Number(mToB(data)), nAfterDot).toString();
				}
				else if (targetUnit == "K")
				{
					return FormatNumber(Number(mToK(data)), nAfterDot).toString();
				}
				else if (targetUnit == "M")
				{
					return FormatNumber(Number(data), nAfterDot).toString();
				}
				return data;
			}
			return data;
		}
		
		public static function bToK(data:String):String
		{
			return (Number(data) / 1024).toString();
		}
		
		public static function bToM(data:String):String
		{
			return (Number(data)/1024/1024).toString();
		}
		
		public static function mToK(data:String):String
		{
			return (Number(data) * 1024).toString();
		}
		
		public static function mToB(data:String):String
		{
			return (Number(data) *1024 * 1024).toString();
		}
		
		public static function kToM(data:String):String
		{
			return (Number(data) * 1024).toString();
		}
		
		public static function kToB(data:String):String
		{
			return (Number(data) * 1024).toString();
		}
		
		/**
		 * 
		 * @param	num			需要转换的数字
		 * @param	nAfterDot	保留小数点位数
		 * @return
		 */
		public static function FormatNumber(num:Number, nAfterDot:int):Number
		{
			var srcStr:String;
			var resultStr:String
			var dotPos:int;
			srcStr = (num).toString();
			srcStr = "" + srcStr + "";
			var strLen:uint = srcStr.length;
			dotPos = srcStr.indexOf(".", 0);
			trace("num="+num);
			trace("nAfterDot="+nAfterDot);
			if (dotPos == -1)
			{
				resultStr = srcStr + ".";
				for (var i:uint = 0; i < nAfterDot; i++)
				{
					resultStr = resultStr + "0";
				}
				return Number(resultStr);
			}
			else
			{
				if ((strLen - dotPos - 1) >= nAfterDot)
				{
					var nAfter:int = dotPos + nAfterDot + 1;
					var nTen:int = 1;
					for (var j:uint = 0; j < nAfterDot; j++)
					{
						nTen = nTen * 10;
					}
					var resultnum:Number = Math.round(num * nTen) / nTen;
					resultStr = resultnum.toString();
					trace("resultStr="+resultStr);
					return Number(resultStr);
				}
				else
				{
					resultStr = srcStr;
					for (i = 0; i < (nAfterDot - strLen + dotPos + 1); i++)
					{
						resultStr = resultStr + "0";
					}
					trace("resultStr="+resultStr);
					return Number(resultStr);
				}
			}
		} //end formatNumber
		
		
		public static function fullZeroWidthPosition(str:String,length:int,position:String="r"):String
		{
			if(str.length<length)
			{
				var bei:int=length-str.length;
				var zero:String="";
				for(var i:int=0;i<bei;i++)
				{
					zero+="0";
				}
				if(position=="r")
				{
					str+=zero;
				}
				else
				{
					str=zero+str;
				}
				
			}
			return str;
		}
		
		
		/**
		 *
		 * @param	num			需要转换的对象
		 * @param	Boundary	转换到多少位
		 * @return
		 */
		public static function fullZero(num:*, Boundary:Number):*
		{
			var length:Number = 0;
			//trace("input num="+num);
			if (num is String)
			{
				
				length = num.length;
			}
			else if (num is Number)
			{
				length = String(num).length;
			}
			if (length < Boundary)
			{
				var temp:Number = Boundary - length;
				var zero:String = "";
				for (var i:int = 0; i < temp; i++)
				{
					zero = zero + "0";
				}
				num = zero + num;
			}
			//trace("output num="+num);
			return num;
		}
		
		
		
		/**
		 * 
		 * @param begin 开始数字
		 * @param end	结束范围
		 * @param step  步进
		 * @return 		返回范围内的一个整数
		 * 
		 */	
		public static function getRoundRandomNumber(begin:uint,end:uint,step:uint=1):uint
		{
			if(begin>end)
			{
				begin=end^(end=begin)*0;//交换begin和end方法一
				//begin = [end, end=begin][0];//交换begin和end方法二
			}
			var arr:Vector.<uint>=new Vector.<uint>();
			var num:uint=0;
			for(var i:uint=begin;i<end+1;i++)
			{
				num=(i-begin)*step+begin;
				if(num>end)
				{
					break;
				}
				arr.push(num);
			}
			arr=arr.concat().sort(function():int{return Math.random()<.5?-1:1});
			if(Math.random()<.5)
			{
				arr.reverse();
			}
			var randomInt:uint=uint(Math.random()*arr.length);
			return arr[randomInt];
		}
		
		/**
		 * 
		 * @param low 开始数字
		 * @param high	结束范围
		 
		 * @return 		返回范围内的一个整数
		 * 
		 */		
		public static function simpleRoundRandom( low:uint, high:uint):uint
		{
			
			return low + Math.random()*100 % (high - low + 1);
		}
		
		public static function getVersion():String
		{
			return "2016.10.20";
		}
		
		public static function getRoundRandomNumberByXipai(begin:uint,end:uint,step:uint=1):uint
		{
			if(begin>end)
			{
				begin=end^(end=begin)*0;//交换begin和end方法一
				//begin = [end, end=begin][0];//交换begin和end方法二
			}
			var arr:Vector.<uint>=new Vector.<uint>();
			var num:uint=0;
			for(var i:uint=begin;i<end+1;i++)
			{
				num=(i-begin)*step+begin;
				if(num>end)
				{
					break;
				}
				arr.push(num);
			}
			xipai(arr,arr.length);
			var randomInt:uint=uint(Math.random()*arr.length);
			return arr[randomInt];
		}
		
		
		public static function hexToByteArray(hex:String,endia:String="little" ):ByteArray 
		{
			var a:ByteArray = new ByteArray;
			if(endia=="little")
			{
				a.endian = Endian.LITTLE_ENDIAN;  
			}
			else
			{
				a.endian = Endian.BIG_ENDIAN;  
			}
			if (hex.length&1==1) hex="0"+hex;
			for (var i:uint=0;i<hex.length;i+=2) {
				a[i/2] = parseInt(hex.substr(i,2),16);
			}
			return a;
		}
		
		
		public static function  xipai(cards:Vector.<uint>,n:uint):void
		{
			// 随机i-1中的任意一个数与i交换
			for (var i:uint = 0; i < n; i++)
			{
				var rand:uint = simpleRoundRandom(0, i);
				var temp:uint = cards[rand];
				cards[rand] = cards[i];
				cards[i] = temp;
			}
		}
		
		
		public static function twoTo16(hex:String):String
		{
			return parseInt(hex,2).toString(16);
		}
		
		public static function twoTo10(hex:String):String
		{
			return parseInt(hex,2).toString(10);
		}
		
	}
}
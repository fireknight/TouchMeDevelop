package k3.utils
{
	import flash.text.Font;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	public class TextMaker
	{
		public function TextMaker()
		{
		}
		
		/**
		 * 
		 * @param _width     宽度
		 * @param _height    高度
		 * @param _fontName  字体名称
		 * @param align      对齐方式 CENTER : String = "center"
		 [静态] 常数；在文本字段内将文本居中对齐。
		 TextFormatAlign
		 END : String = "end"
		 [静态] 常量；将文本与一行的结束边缘对齐。
		 TextFormatAlign
		 JUSTIFY : String = "justify"
		 [静态] 常数；在文本字段内将文本两端对齐。
		 TextFormatAlign
		 LEFT : String = "left"
		 [静态] 常数；在文本字段内将文本左对齐。
		 TextFormatAlign
		 RIGHT : String = "right"
		 [静态] 常数；在文本字段内将文本右对齐。
		 TextFormatAlign
		 START : String = "start"
		 [静态] 常量；将文本与一行的起始边缘对齐。
		 * @param _type		 文本类型 dynamic,input
		 * @param _multiline 是否多行
		 * @param _wordWrap  是否自动换行
		 * @param color		 颜色
		 * @param	size 	字体大小
		 * @param	leading 行距
		 * @param	indent	从左边距到段落中第1个字符的缩进
		 * @param _embed	是否使用嵌入字体
		 * @param space 字符间距	
		 * @return 
		 * 
		 */
		public static function singleText(_width:Number,_height:Number,_fontName:String,align:String,_type:String="dynamic",_multiline:Boolean=true,_wordWrap:Boolean=true,color:Number = 0x181818, size:Number = 18, leading:Number = 3, indent:Number = 30,_embed:Boolean=true,space:Number=0):TextField
		{
			var desTextField:TextField = new TextField();
			desTextField.wordWrap = _wordWrap;
			desTextField.multiline = _multiline;
			desTextField.type = _type;
			//			trace(desTextField.type);
			desTextField.autoSize = align;
			desTextField.embedFonts = _embed;
			if(_type=="input")
			{
				desTextField.selectable = true;
			}
			else
			{
				desTextField.selectable = false;
			}
			desTextField.mouseWheelEnabled = false;
			desTextField.width = _width;
			desTextField.height = _height;
			//			desTextField.border = true;
			var textFormat:TextFormat = new TextFormat();
			textFormat.font = _fontName;
			textFormat.leading = leading;
			textFormat.indent = indent;
			textFormat.size = size;
			textFormat.letterSpacing=space;
			textFormat.align=align;
			textFormat.color = color;
			desTextField.defaultTextFormat = textFormat;
			desTextField.setTextFormat(textFormat);
			return desTextField;
		}
		
		/**
		 *
		 * @param	s		内容
		 * @param	_width
		 * @param	_font	字体
		 * @param	color	字体颜色
		 * @param	size 	字体大小
		 * @param	leading 行距
		 * @param	indent	从左边距到段落中第1个字符的缩进
		 */
		public static function text(s:String, _width:Number, _fontName:String, color:Number = 0x181818, size:Number = 18, leading:Number = 3, indent:Number = 30,_embed:Boolean=true,space:Number=0):TextField
		{
			var desTextField:TextField = new TextField();
			desTextField.wordWrap = true;
			desTextField.multiline = true;
			desTextField.type = "dynamic";
			desTextField.autoSize = TextFieldAutoSize.LEFT;
			desTextField.embedFonts = _embed;
			desTextField.selectable = false;
			desTextField.htmlText = s;
			desTextField.mouseWheelEnabled = false;
			desTextField.width = _width;
			
			//desTextField.border = true;
			var textFormat:TextFormat = new TextFormat();
			textFormat.font = _fontName;
			textFormat.leading = leading;
			textFormat.indent = indent;
			textFormat.size = size;
			textFormat.color = color;
			textFormat.letterSpacing=space;
			desTextField.defaultTextFormat = textFormat;
			desTextField.setTextFormat(textFormat);
			return desTextField;
		}
	}
}
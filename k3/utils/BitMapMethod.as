package k3.utils
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.IBitmapDrawable;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	import flash.utils.Endian;
	
	/**
	 * ...
	 * @author FireKnight
	 */
	public class BitMapMethod
	{
		
		public function BitMapMethod()
		{
			
		}
		
		public static function scale(source:BitmapData,scaleX:Number =1.0,scaleY:Number = 1.0,disposeSource:Boolean = true):BitmapData
		{
			var result:BitmapData = new BitmapData(source.width * scaleX,source.height * scaleY,source.transparent,0xFFFFFF);
			var m:Matrix = new Matrix();
			m.scale(scaleX,scaleY);
			result.draw(source,m);
			if (disposeSource)
				source.dispose()
			return result;
		}
		
		
		/**
		 * 获取按指定尺寸等比例缩放+居中的截图
		 * @param	target        目标对象
		 * @param	tarW          目标尺寸(宽)
		 * @param	tarH          目标尺寸(高)
		 * @param	full          是否显示图片全貌
		 * @return
		 */
		public static function getZoomDraw(target:DisplayObject, tarW:int, tarH:int,full:Boolean=true,dispose:Boolean=true):BitmapData {
			//获取显示对象矩形范围
			var rect:Rectangle = target.getBounds(target);
			//计算出应当缩放的比例
			var perw:Number = tarW / rect.width;
			var perh:Number  = tarH / rect.height;
			var scale:Number  = full?((perw <= perh)?perw:perh):((perw <= perh)?perh:perw);
			//计算缩放后与规定尺寸之间的偏移量
			var offerW:Number  = (tarW - rect.width * scale) / 2;
			var offerH:Number  = (tarH - rect.height * scale) / 2;
			//开始绘制快照（这里透明参数是false,是方便观察效果，实际应用可改为true)
			var bmd:BitmapData = new BitmapData(tarW, tarH,false, 0);
			var matrix:Matrix = new Matrix();
			matrix.scale(scale, scale);
			matrix.translate( offerW, offerH);
			bmd.draw(target, matrix,null,null,null,true);
			//如果是bitmap对象，释放位图资源
			if (target is Bitmap && dispose)   (target as Bitmap).bitmapData.dispose();
			//返回截图数据
			return bmd;
		}
		
		public static function setBitmap(mc:*):Bitmap
		{
			var myBitmapData:BitmapData = new BitmapData(mc.width, mc.height, true, 0x00000000);
			
			myBitmapData.draw(mc);
			
			var myBitMap:Bitmap = new Bitmap(myBitmapData,"auto",true);
			myBitMap.smoothing=true;
			
			return myBitMap;
		}//end setBitmap
		
		
		public static function copy(original:Bitmap):Bitmap
		{
			var myBitmapData:BitmapData = new BitmapData(original.width,original.height,true,0x00000000);
			myBitmapData.draw(original);
			
			var myBitMap:Bitmap = new Bitmap(myBitmapData,"auto",true);
			myBitMap.smoothing=true;
			
			return myBitMap;
		}//end copy
		
		
		public static function draw(container:DisplayObject,rect:Rectangle=null):Bitmap
		{
			
			var source:BitmapData = new BitmapData(container.width,container.height,true,0x00000000);
			source.draw(container);
			if(rect==null)
			{
				rect=new Rectangle(0,0,container.width,container.height);
			}
			var w:Number=rect.width;
			var h:Number=rect.height;
			var myBitmapData:BitmapData = new BitmapData(w,h,true,0x00000000);
			myBitmapData.copyPixels(source,rect, new Point(0, 0));
			var myBitMap:Bitmap = new Bitmap(myBitmapData,"auto",true);
			myBitMap.smoothing=true;
			return myBitMap;
		}
		
		/**
		 * 增加了图片类型尺寸等信息,可直接保存成图片,非压缩版本
		 * @param bmd
		 * @return 
		 * 
		 */
		public static function setBitmapDataToByteArray(bmd:BitmapData):ByteArray
		{
			var bmpWidth:int=bmd.width;
			var bmpHeight:int=bmd.height;
			var imageBytes:ByteArray=bmd.getPixels(bmd.rect);
			var imageSize:int=imageBytes.length;
			var imageDataOffset:int=0x36;
			var fileSize:int=imageSize+imageDataOffset;
			
			//binary bmp data
			var bmpBytes:ByteArray=new ByteArray();
			bmpBytes.endian=Endian.LITTLE_ENDIAN;
			//byte order
			
			//head information
			bmpBytes.length=fileSize;
			bmpBytes.writeByte(0x42);//B
			bmpBytes.writeByte(0x4D);//M
			bmpBytes.writeInt(fileSize);//file size
			bmpBytes.position=0x0A;//offset to image data
			bmpBytes.writeInt(imageDataOffset);
			bmpBytes.writeInt(0x28);//head size
			bmpBytes.position=0x12;//width,height
			bmpBytes.writeInt(bmpWidth);
			bmpBytes.writeInt(bmpHeight);
			bmpBytes.writeShort(1);//planes (1)
			bmpBytes.writeShort(32);// color depth(32 bit);
			bmpBytes.writeInt(0);//copression type
			bmpBytes.writeInt(imageSize);//image data size
			bmpBytes.position=imageDataOffset;//start of image data...
			//write pixel bytes in upside-down order
			// as per BMP format
			var col:int=bmpWidth;
			var row:int=bmpHeight;
			var rowLength:int=col*4;//4 bytes per piexel 32 bit
			try
			{
				//make sure we're starting at the beginning of the image data
				imageBytes.position=0;
				// bottom row up
				while(row--)
				{
					//from end of file up to imageDataOffset
					bmpBytes.position=imageDataOffset+row*rowLength;
					//read through each column writing those bits to the image in normal left to rightorder
					col=bmpWidth;
					while(col--)
					{
						bmpBytes.writeInt(imageBytes.readInt());
					}
				}
			}
			catch(error:Error)
			{
				trace("write bytes error:"+error);
			}
			return bmpBytes;
			
		}
		/**
		 *
		 * 获取位图指定像素位置透明值 
		 * @param target
		 * @param x
		 * @param y
		 * @return 
		 * 
		 */
		public static function getColorAlpha(target:Bitmap,x:Number,y:Number):Number
		{
			var pixelValue:uint;
			var alphaValue:uint;
			var bmpd:BitmapData=target.bitmapData;
			pixelValue = bmpd.getPixel32(x, y);
			alphaValue = pixelValue >> 24 & 0xFF;
			return alphaValue;
		}
		
	}
	
}

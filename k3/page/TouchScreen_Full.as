package k3.page
{
	import flash.display.Sprite;
	import flash.display.StageDisplayState;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.ui.Keyboard;
	import flash.ui.Mouse;
	import flash.utils.setTimeout;
	
	import k3.effect.HitEffectRound;
	import k3.event.KEvent;
	import k3.global.GlobalValue;
	import k3.utils.MyTimer;
	public class TouchScreen_Full extends Sprite
	{
		protected var myTimer:MyTimer;
		protected var waitTime:uint=300;	
		protected var hitColor:uint=0x2B5AA8;
		public function TouchScreen_Full()
		{
			Mouse.hide();
			stage.color=0;
			stage.frameRate=24;
			stage.align="tl";
			stage.scaleMode="noScale";
			stage.displayState=StageDisplayState.FULL_SCREEN_INTERACTIVE;
			this.stage.addEventListener(KeyboardEvent.KEY_UP,handleKeyUp);
			stage.addEventListener("moved",handleMouseMoved);
			stage.addEventListener(MouseEvent.CLICK,handleStageTouched);
		}
		
		protected function stopSeeTouch():void{
		stage.removeEventListener(MouseEvent.CLICK,handleStageTouched);
		}
		
		
		protected function handleMouseMoved(e:KEvent):void
		{
			removeMyTimer();
			addMyTimer();
			
		} 
		
		protected  function handleStageTouched(e:MouseEvent):void
		{
			var hit:HitEffectRound=new HitEffectRound();
			this.addChild(hit);
			hit.x=e.stageX;
			hit.y=e.stageY;
			hit.show(hitColor);
			
			GlobalValue.noticeMove(this);
		}
		
		protected function initTimeout(designWidth:Number):void
		{
			setTimeout(init,1000,designWidth);
		}
		
		protected function init(designWidth:Number):void
		{
			GlobalValue.stageWidth=stage.stageWidth;
			GlobalValue.stageHeight=stage.stageHeight;
			GlobalValue.rat=stage.stageWidth/designWidth;
			showSaverView();
		}
		
		protected function handleKeyUp(e:KeyboardEvent):void
		{
			if(e.keyCode==Keyboard.SPACE)
			{
				Mouse.show();
			}
			else if(e.keyCode==Keyboard.ESCAPE)
			{
				stage.nativeWindow.close();
			}
		}
		
		
		
		protected function addMyTimer():void
		{
			if(!myTimer)
			{
				myTimer=new MyTimer(showSaverView,waitTime);
				myTimer.start();
			}
		}
		
		protected function removeMyTimer():void
		{
			if(myTimer)
			{
				myTimer.GC();
				myTimer=null;
			}
		}
		
		
		
		protected function showSaverView():void
		{
			
		}
	}
}
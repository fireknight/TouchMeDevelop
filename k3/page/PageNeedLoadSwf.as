package k3.page
{
	import flash.display.*;
	import flash.events.*;
	import flash.net.*;
	
	import k3.event.KEvent;
	
	
	
	public class PageNeedLoadSwf extends Sprite
	{
		protected var swfLoader:Loader;
		protected var contentMc:MovieClip;
		public function PageNeedLoadSwf(url:String)
		{
			super();
			swfLoader=new Loader();
			swfLoader.contentLoaderInfo.addEventListener(Event.COMPLETE,handleSwfLoaded);
			
			swfLoader.load(new URLRequest(url));
		}
		
		
		protected function handleSwfLoaded(e:Event):void{
			contentMc=e.target.content;
			this.addChild(contentMc);
			swfLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE,handleSwfLoaded);
			this.dispatchEvent(new KEvent(KEvent.LOADED));
			getChildren();
		}
		
		
		protected function getChildren():void
		{
			
		}
		
		protected function removeLoader():void
		{
			if(swfLoader)
			{
				swfLoader.unloadAndStop();
				swfLoader=null;
			}
		}
		
		
		public function gc():void
		{
			removeLoader();
			
		}
		
	}
}
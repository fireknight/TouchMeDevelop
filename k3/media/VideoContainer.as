package k3.media
{
	/**
	 *	video的容器 
	 */	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.StageVideoAvailabilityEvent;
	import flash.media.StageVideoAvailability;
	import flash.media.Video;
	
	import k3.display.ui.LoadingCircle0;
	import k3.event.KEvent;
	

	
	
	public class VideoContainer extends Sprite
	{
		protected var videoManager:VideoManager;
		protected var max_width:Number;
		protected var max_height:Number;
		protected var nowVideo:*;
		protected var nowVideo_w:Number=-1;
		protected var nowVideo_h:Number=-1;
		protected var ys_video_w:Number;
		protected var ys_video_h:Number;
		protected var _path:String;
		protected var playing:Boolean=false;
		protected var repeat:Boolean=true;
		protected var first:Boolean=true;
		protected var duration:Number;
		protected var fitscreen:Boolean=false;//设置video是否适应屏幕
		protected var middle:Boolean=false;//设置video中心点是否在中间
		protected var color:Number=0xffffff;
		protected var autoPlay:Boolean=false;
		protected var qz:Boolean=false;//是否强制视频大小
		protected var bmp:Bitmap;
		protected var bmpd:BitmapData;
		protected var bmps:Vector.<Bitmap>;
		protected var center:Boolean=false;//是否置于中间位置
		protected var loadingMc:LoadingCircle0;
		public static const INIT	:String="movieSizeGeted";
		public static const PLAY_END:String="playEnd";
		//public static const BUFFER_EMPTY:String="BufferEmpty";
		//public static const BUFFER_FULL:String="BufferFull";
		public static const PLAY_PROGRESS:String="playTime";
		
		
		
		
		
		public function VideoContainer(w:Number=0,h:Number=0,_middle:Boolean=false,_fitscreen:Boolean=false,_color:Number=0xffffff,alpha:Number=1,_qz:Boolean=false,_center:Boolean=true)
		{
			trace("videoContainer 2022.05.13 build 10");
			max_width=w;
			max_height=h;
			middle=_middle;
			fitscreen=_fitscreen;
			center=_center;
			qz=_qz;
			if(fitscreen)
			{
				this.graphics.beginFill(_color,alpha);
				this.graphics.drawRect(0,0,w,h);
				this.graphics.endFill();
			}
			
			this.mouseEnabled=false;
			this.mouseChildren=false;
			this.addEventListener(Event.ADDED_TO_STAGE,handleAddedToStage);
		}
		
		protected function handleAddedToStage(e:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE,handleAddedToStage);
			
		}
		
		
		public function get isPlaying():Boolean
		{
			return playing;
		}
		
		public function get path():String
		{
			return _path;
		}
		
		public function set path(value:String):void
		{
			_path = value;
		}
		
		public function showMovie(s:String,_repeat:Boolean=true,_auto:Boolean=false):void
		{
			repeat=_repeat;
			if(path==s)return;
			path=s;
			if(playing)
			{
				removeMovie();
			}
			autoPlay=_auto;
			play();
		}
		
		public function play():void
		{
			playMovie(path);
		}
		
		protected function addLoadingMc():void
		{
			if(!loadingMc)
			{
				loadingMc=new LoadingCircle0();
				this.addChild(loadingMc);
				loadingMc.x=max_width/2-loadingMc.width/2;
				loadingMc.y=max_height/2-loadingMc.height/2;
			}
		}
		
		protected function removeLoadingMc():void
		{
			if(loadingMc)
			{
				this.removeChild(loadingMc);
				loadingMc=null;
			}
		}
		
		protected function removeMovie():void
		{
			if(!videoManager)return;
			
			videoManager.removeEventListener(VideoManager.PLAYER_STATE_CHANGED,handleMovieEnd);
			videoManager.removeEventListener(VideoManager.METADATA_READY,handleGetSize);
			videoManager.removeEventListener(VideoManager.CONNECTION_READY,handleConnectReady);
			//videoManager.removeEventListener("BufferEmpty",handleBuffEvent);
			//videoManager.removeEventListener("BufferFull",handleBuffEvent);
			if(this.hasEventListener(Event.ENTER_FRAME))
			{
				this.removeEventListener(Event.ENTER_FRAME,handleNsEnterFrame);
			}
			reset();
			videoManager.gc();
			videoManager=null;
			playing=false;
		}
		
		protected function reset():void
		{
			nowVideo=null;
			nowVideo_w=-1;
			nowVideo_h=-1;
		}
		
		public function restart():void
		{
			this.removeLoadingMc();
			playing=true;
			videoManager.resume();
			videoManager.gotoAndPlay(1);
			videoManager.play();
			if(!this.hasEventListener(Event.ENTER_FRAME))
			{
				this.addEventListener(Event.ENTER_FRAME,handleNsEnterFrame);
			}
		}
		
		
		public function gotoAndPlay(s:uint):void
		{
			this.removeLoadingMc();
			playing=true;
			videoManager.gotoAndPlay(s);
			if(!this.hasEventListener(Event.ENTER_FRAME))
			{
				this.addEventListener(Event.ENTER_FRAME,handleNsEnterFrame);
			}
		}
		
		protected function playMovie(s:String):void
		{
			playing=true;
			if(!videoManager)
			{
				videoManager=new VideoManager();
				videoManager.addEventListener(VideoManager.PLAYER_STATE_CHANGED,handleMovieEnd);
				videoManager.addEventListener(VideoManager.CONNECTION_READY,handleConnectReady);
				
			}
			
				videoManager.showMovie(s);
		
				
				if(videoManager._video&&!this.contains(videoManager._video))
				{
					this.addChild(videoManager._video);
				}
				videoManager.addEventListener(VideoManager.METADATA_READY,handleGetSize);
				//videoManager.addEventListener("BufferEmpty",handleBuffEvent);
				//videoManager.addEventListener("BufferFull",handleBuffEvent);
			
			this.addEventListener(Event.ENTER_FRAME,handleNsEnterFrame);
		}
		
		protected function handleConnectReady(e:KEvent):void
		{
			nowVideo=videoManager._video;
			this.addChild(videoManager._video);
			trace(nowVideo);
			trace("handleConnectReady");
		
		}
		
		protected function handleMovieEnd(e:KEvent):void
		{
			if(videoManager.getPlayerState()==0)
			{
				if(repeat)
				{
					videoManager.gotoAndPlay(0);
		
					
				}
				else
				{
					this.dispatchEvent(new KEvent(VideoContainer.PLAY_END,null,true));
					if(this.hasEventListener(Event.ENTER_FRAME))
					{
						this.removeEventListener(Event.ENTER_FRAME,handleNsEnterFrame);
					}
				}
				
			}
			
		}
		
		public function updateVideoPosition():void
		{
			if(middle)
			{
				nowVideo.x=-nowVideo.width/2;
				nowVideo.y=-nowVideo.height/2;
			}
			else
			{
				nowVideo.x=(max_width-nowVideo.width)>>1;
				nowVideo.y=(max_height-nowVideo.height)>>1;
			}
			
		}
		
		/*protected function handleBuffEvent(e:KEvent):void
		{
			if(e.type=="BufferEmpty")
			{
				trace(Math.ceil(duration),Math.ceil(videoManager.time));
				if(Math.ceil(duration)==Math.ceil(videoManager.time))
				{
					this.dispatchEvent(new KEvent(VideoContainer.BUFFER_EMPTY,null,true));
				}
				else
				{
					this.addLoadingMc();
				}
				this.dispatchEvent(new KEvent(VideoContainer.BUFFER_EMPTY,null,true));
				
			}
			else
			{
				this.removeLoadingMc();
				this.dispatchEvent(new KEvent(VideoContainer.BUFFER_FULL,null,true));
			}
		}*/
		
		
		
		protected function handleGetSize(e:KEvent):void
		{
			nowVideo_w=ys_video_w=e.obj.width;
			nowVideo_h=ys_video_h=e.obj.height;
			duration=e.obj.duration;
			trace("影片有"+duration+"秒");
			nowVideo.width=nowVideo_w;
			nowVideo.height=nowVideo_h;
			
			if(qz)
			{
				nowVideo.width=max_width;
				nowVideo.height=max_height;
				nowVideo_w=max_width;
				nowVideo_h=max_height;
			}
			if(fitscreen)
			{
				fitScreen();
				
			}
			if(center)
			{
				updateVideoPosition();
			}
			var info:VideoInfo=new VideoInfo();
			info.duration=duration;
			info.width=nowVideo_w;
			info.height=nowVideo_h;
			this.dispatchEvent(new KEvent(VideoContainer.INIT,info));
			if(first)
			{
				first=false;
				if(!autoPlay)
				{
					videoManager.gotoAndPlay(1);
					videoManager.pause();
					playing=false;
					if(this.hasEventListener(Event.ENTER_FRAME))
					{
						this.removeEventListener(Event.ENTER_FRAME,handleNsEnterFrame);
					}
				}
				
			}
		}
		
		public function qzFitlScreen():void
		{
			qz=true;
			nowVideo.width=max_width;
			nowVideo.height=max_height;
			nowVideo_w=max_width;
			nowVideo_h=max_height;
			if(center)
			{
				updateVideoPosition();
			}
		}
		
		public function fitScreen():void
		{
			qz=false;
			nowVideo_w=ys_video_w;
			nowVideo_h=ys_video_h;
			nowVideo.width=nowVideo_w;
			nowVideo.height=nowVideo_h;
			
			var rat:Number = nowVideo.width / nowVideo.height;
			var targetW:Number;
			var targetH:Number;
			if (nowVideo_w >= nowVideo_h)
			{
				targetW = max_width;
				targetH = max_width / rat;
				if (targetH > max_height)
				{
					targetH = max_height;
					targetW = max_height * rat;
				}
			}
			else
			{
				targetH = max_height;
				targetW = max_height * rat;
				if (targetW > max_width)
				{
					targetW = max_width;
					targetH = max_width / rat;
				}
				
			}
			
			nowVideo.width=targetW;
			nowVideo.height=targetH;
			trace(nowVideo.x,nowVideo.y,nowVideo.width,nowVideo.height);
			if(center)
			{
				updateVideoPosition();
			}
			
		}
		
		public function get video():Video
		{
			return nowVideo;
		}
		
		protected function handleNsEnterFrame(e:Event):void
		{
			this.dispatchEvent(new KEvent(VideoContainer.PLAY_PROGRESS,videoManager.time,true));
			//			trace("nowVideo.width="+nowVideo.width);
			//			trace("nowVideo.height="+nowVideo.height);
		}
		
		public function totalTime():uint
		{
			return Math.ceil(duration);
		}
		
		public function setVolume(s:Number):void
		{
			videoManager.setVolume(s);
		}
		
		public function resume():void
		{
			this.removeLoadingMc();
			trace("RESUME");
			playing=true;
			videoManager.resume();
			if(videoManager._video)
			{
				if(!videoManager._video.parent)
				{
					this.addChild(videoManager._video);
				}
				
			}
			if(!this.hasEventListener(Event.ENTER_FRAME))
			{
				this.addEventListener(Event.ENTER_FRAME,handleNsEnterFrame);
			}
		}
		
		
		public function pause():void
		{
			this.removeLoadingMc();
			trace("PAUSE");
			playing=false;
			videoManager.pause();
			if(this.hasEventListener(Event.ENTER_FRAME))
			{
				this.removeEventListener(Event.ENTER_FRAME,handleNsEnterFrame);
			}
		}
		
		
		public function stop():void
		{
			this.removeLoadingMc();
			if(!playing)return;
			playing=false;
			videoManager.stop();
			if(this.hasEventListener(Event.ENTER_FRAME))
			{
				this.removeEventListener(Event.ENTER_FRAME,handleNsEnterFrame);
			}
		}
		
		protected function addBmp(stop:Boolean=false):void
		{
			if(!bmps)
			{
				bmps=new Vector.<Bitmap>();
			}
			if(bmp)
			{
				if(this.contains(bmp))
				{
					this.removeChild(bmp);
				}
			}
			bmpd=new BitmapData(max_width,max_height,true);
			try
			{
				bmpd.draw(this);
				bmp=new Bitmap(bmpd);
				
				bmps.push(bmp);
				removeLastBmp();
			}
			catch(e:Error)
			{
				trace(e);
				trace("catch="+this.path);
			}
			
			if(stop)
			{
				videoManager.stop();
			}
		}
		
		protected function removeLastBmp():void
		{
			if(bmps)
			{
				if(bmps.length>1)
				{
					var _bm:Bitmap=bmps[0];
					var _bmpd:BitmapData;
					if(_bm)
					{
						if(this.contains(_bm))
						{
							this.removeChild(_bm);
						}
						_bmpd=_bm.bitmapData;
						_bmpd.dispose();
						_bm=null;
					}
					
					bmps.splice(0,1);
				}
			}
			
		}
		
		public function getBitmap():Bitmap
		{
			if(!bmps)
			{
				addBmp();
			}
			return bmps[bmps.length-1];
		}
		
		protected function removeBmp():void
		{
			if(bmp)
			{
				if(this.contains(bmp))
				{
					this.removeChild(bmp);
				}
			}
			if(bmps)
			{
				if(bmps.length>0)
				{
					var _bm:Bitmap;
					var _bmpd:BitmapData;
					var length:uint=bmps.length;
					for(var i:uint=0;i<length;i++)
					{
						_bm=bmps[i];
						if(_bm)
						{
							if(this.contains(_bm))
							{
								this.removeChild(_bm);
							}
							_bmpd=_bm.bitmapData;
							_bmpd.dispose();
						}
					}
				}
				bmps=null;
			}
		}
		
		public function gc():void
		{
			this.removeLoadingMc();
			playing=false;
			removeMovie();
			if(bmpd)
			{
				bmpd.dispose();
			}
			removeBmp();
		}
	}
}
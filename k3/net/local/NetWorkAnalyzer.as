package k3.net.local
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import k3.event.KEvent;
	import flash.net.InterfaceAddress;
	import flash.net.NetworkInterface;
	public final class NetWorkAnalyzer extends EventDispatcher
	{
		public const NETWORK_NORMAL:String = "NETWORK_NORMAL";
		public const NETWORK_ABNORMAL:String = "NETWORK_ABNORMAL";
		private var infos:Array;
		public function NetWorkAnalyzer(target:IEventDispatcher=null)
		{
			super(target);
		}
		
		
		public function analyze(data:Vector.<NetworkInterface>):void
		{
			var data_length:int=data.length;
			
			if(infos)
			{
				infos=null;
			}
			infos=new Array();
			for(var i:uint=0;i< data_length;i++)
			{
				var _net:NetworkInterface= data[i] ;
				var _hardwareAddress:String=_net.hardwareAddress;
				
				if(_hardwareAddress.length>0)
				{
					infos.push(_net);
				}
			}
			if(infos.length==0)
			{
				this.dispatchEvent(new KEvent(this.NETWORK_ABNORMAL,"未找到可用的网卡"));
				return;
			}
			getActiveNetWork();
		}
		
		
		private function getActiveNetWork():void
		{
			var data_length:int=infos.length;
			
			var activeNet:NetworkInterface;
			var _net:NetworkInterface;
			for(var i:uint=0;i<data_length;i++)
			{
				_net= infos[i] as NetworkInterface;
				
				if(_net.mtu>0 && _net.active)
				{
					activeNet=_net;
					break;
				}
			}
			if(activeNet)
			{
				
				this.dispatchEvent(new KEvent(this.NETWORK_NORMAL,activeNet.displayName+":"+activeNet.addresses[0].address));
			}
			else
			{
				this.dispatchEvent(new KEvent(this.NETWORK_ABNORMAL,"未找到可用的连接"));
			}
		}
		
	}
}
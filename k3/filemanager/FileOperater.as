package k3.filemanager
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.utils.ByteArray;	
	public class FileOperater extends EventDispatcher
	{
		public function FileOperater()
		{
			
		}
		
		/**
		 * 打开文件
		 * @param file
		 * @return
		 *
		 */
		public static function openFileToByte(file:File):ByteArray
		{
			if (!file || !file.exists)
			{
				return null;
			}
			var stream:FileStream = new FileStream();
			stream.open(file, FileMode.READ);
			var fileDate:ByteArray = new ByteArray();
			stream.readBytes(fileDate, 0, stream.bytesAvailable);
			stream.close();
			return fileDate;
		}
		
		/**
		 * 文本方式打开文件
		 * @param file
		 * @param type
		 * @return
		 *
		 */
		public static function openAsTxt(file:File, type:String = null):String
		{
			var b:ByteArray = openFileToByte(file);
			if (!b)
			{
				return null;
			}
			trace(b.bytesAvailable);
			b.position = 0;
			type = type ? type : getFileEncode(b);
			//trace("type="+type);
			b.position = 0;
			return b.readMultiByte(b.bytesAvailable, type);
		}
		
		public static function openAsUtfTxt(file:File, type:String = null):String
		{
			var b:ByteArray = openFileToByte(file);
			if (!b)
			{
				return null;
			}
			b.position = 0;
			return b.readMultiByte(b.bytesAvailable, "utf-8");
		}
		
		/**
		 * object方式打开文件 数据可以保存为amf格式
		 * @param file
		 * @param inflate
		 * @return
		 *
		 */
		public static function openAsObj(file:File, inflate:Boolean = false):*
		{
			var b:ByteArray = openFileToByte(file);
			if (!b)
			{
				return null;
			}
			b.position = 0;
			
			if (inflate)
			{
				b.inflate();
				b.position = 0;
			}
			return b.readObject();
		}
		
		public static function getFileEncode(bytes:ByteArray):String
		{
			var b0:int = bytes.readUnsignedByte();
			trace("b0="+b0.toString(16));
			
			var b1:int = bytes.readUnsignedByte();
			trace("b1="+b1.toString(16));
			bytes.position = 0;
			if (b0 == 0xFF && b1 == 0xFE)
			{
				return "unicode";
			}
			else if (b0 == 0xFE && b1 == 0xFF)
			{
				return "unicodeFFFE";
			}
			else if (b0 == 0xEF && b1 == 0xA0)
			{
				return "utf-8";
			}
			else if (b0 == 0xE6 && b1 == 0xA0)
			{
				return "utf-8";
			}
			else
			{
				return "gb2312"
			}
			
		}
		
		/**
		 * 写文件
		 * @param data
		 * @param path
		 * @return
		 *
		 */
		public static function write(data:*, path:String,_file:File=null):File
		{
			
			var stream:FileStream;
			var byte:ByteArray
			if (data is ByteArray)
			{
				byte = data as ByteArray;
			}
			else if (data is String)
			{
				byte = new ByteArray();
				byte.writeUTFBytes(data);
			}
			else
			{
				byte = new ByteArray();
				byte.writeObject(data);
			}
			
			byte.position = 0;
			
			try
			{
				if(_file!=null)
				{
					
				}
				else
				{
					_file = new File(path);
				}
				
				stream = new FileStream();
				stream.open(_file, FileMode.WRITE);
				stream.writeBytes(byte);
				stream.close();
				return _file;
			}
			catch (e:Error)
			{
				trace("write file failed");
				
			}
			return null;
		}
		
		
	}
}
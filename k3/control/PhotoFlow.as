package k3.control
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import com.greensock.TweenLite;
	import com.greensock.easing.Quint;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import k3.display.pic.KBitmap;
	import k3.effect.Reflection;
	public class PhotoFlow extends EventDispatcher
	{
		protected var canView:uint = 5;
		protected var startX:Number;
		protected var endX:Number;
		protected var pics:Array;
		protected var middleScale:Number = 1;
		protected var secondScale:Number = 0.8;
		protected var normalScale:Number = 0.5;
		protected var _cur:Number=0;
		protected var space:Number = 10;
		protected var centerX:Number;
		protected var centerY:Number;
		public function PhotoFlow(thumbs:*,_left:Number,_right:Number,_centerX:Number,_centerY:Number,target:IEventDispatcher=null)  
		{
			super(target);
			
			
			startX = _left;
			endX = _right;
			centerX = _centerX;
			centerY = _centerY;
			handleSprite(thumbs);
		}
		
		protected function handleSprite(s:*):void
		{
			pics = new Array();
			for (var i:uint = 0; i < s.length; i++)
			{
				var upBitmap:Bitmap = getUpBitmap(s[i]);
				var bottomBitmap:Reflection = getBottomBitmap(s[i]);
				upBitmap.x = -upBitmap.width / 2;
				upBitmap.y = -upBitmap.height;
				bottomBitmap.x= -upBitmap.width / 2;
				bottomBitmap.y = 5;
				s[i].y = centerY;
				pics.push({sprite:s[i],maxWidth:upBitmap.width});
			}
			positionArray();
		}
		
		protected function getUpBitmap(s:Sprite):Bitmap
		{
			
			var c:Bitmap;
			for (var i:uint = 0; i < s.numChildren; i++)
			{
				if (s.getChildAt(i) is Bitmap)
				{
					c = s.getChildAt(i) as Bitmap;
					break;
				}
			}
			return c;
		}
		
		protected function getBottomBitmap(s:Sprite):Reflection
		{
			var c:Reflection;
			for (var i:uint = 0; i < s.numChildren; i++)
			{
				if (s.getChildAt(i) is Reflection)
				{
					c = s.getChildAt(i) as Reflection;
					break;
				}
			}
			return c;
		}
		
		public function positionArray():void
		{
			for (var i:uint = 0; i < pics.length; i++)
			{
				if (i == _cur)
				{
					TweenLite.to(pics[i].sprite, 0.5, { alpha:1, x:centerX, scaleX:middleScale,scaleY:middleScale,ease:Quint.easeOut } );
				}
				else if (i < _cur)
				{
					if (_cur - i ==1)
					{
						TweenLite.to(pics[i].sprite, 0.5, { 
							alpha:1,
							x:getPosition1(), 
							scaleX:secondScale,scaleY:secondScale,ease:Quint.easeOut } );
					}
					else if (_cur - i == 2)
					{
						TweenLite.to(pics[i].sprite, 0.5, { 
							alpha:1,
							x:getPosition0(), 
							scaleX:normalScale,scaleY:normalScale,ease:Quint.easeOut } );
					}
					else
					{
						TweenLite.to(pics[i].sprite, 0.5, { 
							alpha:0,
							x:startX, 
							scaleX:normalScale,scaleY:normalScale,ease:Quint.easeOut } );
					}
				}
				else if (i > _cur)
				{
					if (i - _cur ==1)
					{
						TweenLite.to(pics[i].sprite, 0.5, { 
							alpha:1,
							x:getPosition2(), 
							scaleX:secondScale,scaleY:secondScale,ease:Quint.easeOut } );
					}
					else if (i - _cur == 2)
					{
						
						TweenLite.to(pics[i].sprite, 0.5, { 
							alpha:1,
							x:getPosition3(), 
							scaleX:normalScale,scaleY:normalScale,ease:Quint.easeOut } );
					}
					else
					{
						TweenLite.to(pics[i].sprite, 0.5, { 
							alpha:0,
							x:endX,
							scaleX:normalScale,scaleY:normalScale,ease:Quint.easeOut } );
					}
				}
			}
		}
		
		protected function getPosition0():Number
		{
			return getPosition1()-pics[_cur-2].maxWidth*normalScale/2 - space - pics[_cur-1].maxWidth*secondScale/2;
		}
		
		protected function getPosition1():Number
		{
			return centerX - pics[_cur].maxWidth/2-  pics[_cur-1].maxWidth*secondScale/2-space;
		}
		
		protected function getPosition2():Number
		{
			return centerX + pics[_cur].maxWidth/2 + space+  pics[_cur+1].maxWidth*secondScale/2;
		}
		
		protected function getPosition3():Number
		{
			return getPosition2() + pics[_cur + 1].maxWidth * secondScale + space + pics[_cur + 2].maxWidth * normalScale / 2;
		}
		
		
		public function set curNum(s:Number):void
		{
			_cur = s;
			positionArray();
		}
		
		public function get curNum():Number
		{
			return _cur;
		}
	}
}
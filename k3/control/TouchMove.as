package k3.control
{
	import flash.display.DisplayObjectContainer;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.utils.getTimer;
	import k3.global.Time;
	import k3.event.KEvent;
	public class TouchMove extends EventDispatcher
	{
		protected var touchTime:Number=0;
		protected const touchEndTime:Number=600;//触摸结束时如果小于这个时间
		protected var dragDistance:Number=20;//触摸结束时如果大于这个距离  判定为要换图
		protected var startX:Number;
		private var closed:Boolean=false;
		protected var startMouseX:Number;
		protected var parent:DisplayObjectContainer;
		public var moveDisatance:Number;
		public var moved:Boolean=false;
		public static const FLIPED:String="fliped";
		public function TouchMove(target:DisplayObjectContainer)
		{
			parent=target;
			parent.stage.addEventListener(MouseEvent.MOUSE_DOWN,handleMouseDown);
		}
		
		public function set _dragDistance(value:Number):void
		{
			dragDistance=value;
		}
		
		public function get isClick():Boolean
		{
			return Math.abs(moveDisatance)<dragDistance;
		}
		
		protected function handleMouseDown(e:MouseEvent):void
		{
			trace(Time.hour_minute_second+":鼠标按下");
			if(closed)return;
			moved=false;
			touchTime=getTimer();
			startMouseX=e.stageX;
			parent.stage.addEventListener(MouseEvent.MOUSE_UP,handleMouseUp);
			parent.stage.addEventListener(MouseEvent.MOUSE_MOVE,handleMouseMove);
		}
		
		protected function handleMouseMove(e:MouseEvent):void
		{
			if(closed)return;
			var endMouseX:Number=e.stageX;
			var distance:Number=e.stageX-startMouseX;
			moveDisatance=endMouseX-startMouseX;
			if(Math.abs(moveDisatance)>dragDistance)
			{
				handleMove();
			}
			
		}
		
		private function handleMove():void
		{
			moved=true;
			var dir:String;
			//确定是要切换图片了,现在判断方向
			if(moveDisatance>0)
			{
				dir="right";
			}
			else
			{
				dir="left";
			}
			trace(Time.hour_minute_second+":翻页 方向是  "+dir);
			this.dispatchEvent(new KEvent(TouchMove.FLIPED,dir));
			startMouseX=NaN;
			moveDisatance=NaN;
			if(parent.stage)
			{
				parent.stage.removeEventListener(MouseEvent.MOUSE_UP,handleMouseUp);
				parent.stage.removeEventListener(MouseEvent.MOUSE_MOVE,handleMouseMove);
			}
		}
		
		
		
		protected function handleMouseUp(e:MouseEvent):void
		{
			trace(Time.hour_minute_second+": 鼠标抬起");
			if(closed)return;
			var endMouseX:Number=e.stageX;
			var nowTime:uint=getTimer();
			
			//===========
			if(nowTime-touchTime<touchEndTime)
			{
				moveDisatance=endMouseX-startMouseX;
				//如果是要快速切换
				if(Math.abs(moveDisatance)>dragDistance)
				{
					handleMove();
					return;
				}
			}
			if(parent.stage)
			{
				parent.stage.removeEventListener(MouseEvent.MOUSE_UP,handleMouseUp);
				parent.stage.removeEventListener(MouseEvent.MOUSE_MOVE,handleMouseMove);
			}
		}
		
		public function gc():void
		{
			closed=true;
			if(parent)
			{
				if(parent.stage)
				{
					if(parent.stage.hasEventListener(MouseEvent.MOUSE_DOWN))
					{
						parent.stage.removeEventListener(MouseEvent.MOUSE_DOWN,handleMouseDown);
					}
				}
				
			}
		}
	}
}
package k3.global
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.utils.getTimer;
	
	import k3.utils.MyTimer;

	public class CountTime extends EventDispatcher
	{
		private var myTimer:MyTimer;
		private var waitTime:Number=0.1;
		private var passTime:Number=0;
		public function CountTime(target:IEventDispatcher=null)
		{
			super(target);
		}
		
		public function start():void
		{
			if(!myTimer)
			{
				myTimer=new MyTimer(recode,waitTime);
				myTimer.start();
			}
		}
		
		public function pause():void
		{
			
			myTimer.stop();
		}
		
		public function resume():void
		{
			myTimer.start();
		}
		
		private function recode():void
		{
			passTime+=waitTime;
			
		}
		
		private function removeMyTimer():void
		{
			if(myTimer)
			{
				myTimer.GC();
				myTimer=null;
			}
		}
		
		public function stop():uint
		{
			removeMyTimer();
			return passTime;
		}
	}
}
package k3.global
{
	public class Time
	{
		public function Time()
		{
		}
		public static function get hour_minute_second():String
		{
			var date:Date=new Date();
			return date.hours.toString()+":"+date.minutes.toString()+":"+date.seconds.toString();
		}
		public static function getNowTime():String
		{
			var date:Date=new Date();
			return (date.month+1).toString()+"/"+date.day+" "+date.hours+"."+date.minutes+":"+date.milliseconds;
		}
		public static function millonsTransToHour(ms:int):int
		{
			return  int((ms/3600000)%24);
		}
		
		public static function millonsTransToMinute(ms:int):int
		{
			return  int((ms/60000)%60);
		}
		
		
		public static function millonsTransToSecond(ms:int):int
		{
			return  int((ms/1000)%60);
		}
	}
}
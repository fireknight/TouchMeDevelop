package k3.global
{
	import flash.display.DisplayObjectContainer;
	
	import k3.event.KEvent;

	public class GlobalValue
	{
		
		private static var _stageWidth:uint;
		private static var _stageHeight:uint;
		private static var _rat:Number;
		
		public function GlobalValue()
		{
		}
		
		public static function noticeMove(container:DisplayObjectContainer):void	
		{
			container.dispatchEvent(new KEvent(KEvent.MOVED,null,true,false));
		}
		
		public static function get rat():Number
		{
			return _rat;
		}
		
		public static function set rat(value:Number):void
		{
			_rat = value;
		}
		
		public static function get stageHeight():uint
		{
			return _stageHeight;
		}
		
		public static function set stageHeight(value:uint):void
		{
			_stageHeight = value;
		}
		
		public static function get stageWidth():uint
		{
			return _stageWidth;
		}
		
		public static function set stageWidth(value:uint):void
		{
			_stageWidth = value;
		}

	}
}
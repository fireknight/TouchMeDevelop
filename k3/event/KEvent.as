package k3.event 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author FireKnight
	 */
	public class KEvent extends Event 
	{
		public var obj:*;
		public static const CLOSE:String="close";
		public static const OPEN:String="open";
		public static const MOVED:String="moved";
		public static const LOADED:String="loaded";
		public static const IOERROR:String="ioerror";
		public static const PAGE_EXIT:String="page_exit";
		public static const PAGE_BACK:String="page_back";
		public function KEvent(type:String,s:*=null,bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			obj = s;
			
		}
		
		public override function clone():Event
		{
			
			return new KEvent(type,obj,bubbles, cancelable);
		}
		
	}

}
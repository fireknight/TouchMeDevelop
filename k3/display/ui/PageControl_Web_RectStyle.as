package k3.display.ui
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import k3.event.KEvent;
	import k3.global.Time;	
	public class PageControl_Web_RectStyle extends Sprite
	{
		public static const PAGECHANGE:String="pageChange";
		protected var parentWidth:Number;
		protected var parentHeight:Number;
		protected var pageTotal:uint;
		protected var curNum:int=0;
		protected var rects:Vector.<PageControlRect>;
		protected var align:uint=0; //0:下中 1:上中
		protected var rect_width:Number=22;
		protected var rect_height:Number=6;
		protected var rectNormalColor:Number=0xffffff;
		protected var rectChosedColor:Number=0;
		protected var normalAlpha:Number;
		protected var chosedAlpha:Number;
		protected var gap:Number=20;
		public function PageControl_Web_RectStyle()
		{
			super();
		}
		
		
		
		/**
		 * 
		 * @param total			总数
		 * @param _parentW		场景宽
		 * @param _parentH		场景高
		 * @param _alinMode		0:下中 1:右 2:左 3:上
		 * @param _size			球直径
		 * @param normalColor	普通颜色
		 * @param chosedColor	被选择颜色
		 * 
		 */
		public function init(total:uint,_parentW:Number,_parentH:Number,_alinMode:uint=0,_width:Number=22,_height:Number=6,normalColor:Number=0xffffff,_normalAlpha:Number=1,chosedColor:Number=0,_chosedAlpha:Number=1):void
		{
			align=_alinMode;
			pageTotal=total;
			parentWidth=_parentW;
			parentHeight=_parentH;
			rect_width=_width;
			rect_height=_height
			rectNormalColor=normalColor;
			rectChosedColor=chosedColor;
			normalAlpha=_normalAlpha;
			chosedAlpha=_chosedAlpha;
			initBalls();
		}
		
		
		protected function initBalls():void
		{
			rects=new Vector.<PageControlRect>();
			var rect:PageControlRect;
			for(var i:uint=0;i<pageTotal;i++)
			{
				rect=new PageControlRect();
				this.addChild(rect);
				rect.init(rect_width,rect_height,rectNormalColor,normalAlpha,rectChosedColor,chosedAlpha);
				rect.x=i*(rect_width+gap);
				rects.push(rect);
				rect.name=i.toString();
				rect.addEventListener(MouseEvent.CLICK,handleRectClick);
			}
			rects[curNum].chosed();
			makeRects();
		}
		
		protected function makeRects():void
		{
			var curWidth:Number;
			var curHeight:Number;
			curWidth=pageTotal*rect_width+gap*(pageTotal-1);
			curHeight=rect_height;
			this.x=parentWidth/2-curWidth/2;
			if(align==0)
			{
				
			
				this.y=parentHeight-gap-curHeight;
				
				
			}
			var rect:PageControlRect;
			for(var i:uint=0;i<pageTotal;i++)
			{
				rect=rects[i];
				rect.x=i*(rect_width+gap);
				
			}
		}
		
		protected function handleRectClick(e:MouseEvent):void
		{
			
			var rect:PageControlRect=PageControlRect(e.currentTarget);
			trace(Time.hour_minute_second+":rect  "+rect.name+" 点击了");
			var num:uint=uint(rect.name);
			if(num==curNum)return;
			rect[curNum].normal();
			
			curNum=num;
			rects[curNum].chosed();
			this.dispatchEvent(new KEvent(PageControl_Web_RectStyle.PAGECHANGE,num));
		}
		
		public function changePage(num:uint):void
		{
			if(num==curNum)return;
			rects[curNum].normal();
			
			curNum=num;
			rects[curNum].chosed();
		}
		
		protected function removeRectEvent():void
		{
			if(rects)
			{
				var rect:PageControlRect;
				for(var i:uint=0;i<pageTotal;i++)
				{
					rect=rects[i];
					rect.removeEventListener(MouseEvent.CLICK,handleRectClick);
				}
				rects=null;
			}
		}
		
		public function gc():void
		{
			removeRectEvent();
		}
		
	}
}
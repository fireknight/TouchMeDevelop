package k3.display.ui
{
	import flash.display.Sprite;
	
	public class PageControlCircle extends Sprite
	{
		private var radius:Number;
		private var normalColor:Number;
		private var chosedColor:Number;
		private var lineColor:Number;
		private var normalAlpha:Number;
		private var chosedAlpha:Number;
		private var state:uint=0;
		public function PageControlCircle()
		{
			super();
			this.buttonMode=true;
		}
		
		public function drawSize(_ra:Number):void
		{
			radius=_ra;
			refresh();
		}
		
		public function  setLineColor(num:Number):void
		{
			lineColor=num;
			refresh();
		}
		
		public function setNormalColor(num:Number,a:Number):void
		{
			normalColor=num;
			normalAlpha=a;
			refresh();
		}
		
		public function setChosedColor(num:Number,a:Number):void
		{
			chosedColor=num;
			chosedAlpha=a;
			refresh();
		}
		
		
		public function init(_ra:Number,_normal:Number,_normalAlpha:Number,_chosed:Number,_chosedAlpha:Number,_line:Number):void
		{
			drawSize(_ra);
			setNormalColor(_normal,_normalAlpha);
			setChosedColor(_chosed,_chosedAlpha);
			setLineColor(_line);
			normal();
		}
		
		public function refresh():void
		{
			this.graphics.clear();
			this.graphics.lineStyle(1,lineColor);
			if(state==0)
			{
				this.graphics.beginFill(normalColor,normalAlpha);
				this.mouseEnabled=true;
			}
			else
			{
				this.graphics.beginFill(chosedColor,chosedAlpha);
				this.mouseEnabled=false;
			}
			
			this.graphics.drawCircle(0,0,radius);
		
			this.graphics.endFill();
			
			
			
		}
		
		public function normal():void
		{
			state=0;
			refresh();
			
		}
		public function chosed():void
		{
			state=1;
			refresh();
		}
	}
}
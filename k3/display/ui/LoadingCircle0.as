package k3.display.ui
{
	import flash.display.Sprite;
	import flash.display.Shape;
	import flash.events.*;
	import flash.utils.*;	
	public final class LoadingCircle0 extends Sprite
	{
		private var timer:Timer;
		private var slices:int;
		private var radius:int;
		private var slice_x:Number;
		private var slice_y:Number;
		private var slice_width:Number;
		private var slice_height:Number;
		private var slice_ellipseWidth:Number;
		private var slice_ellipseHeight:Number;
		private var slice_color:Number;
		public function LoadingCircle0(slices:int=12, radiurs:int=6, slice_x:Number = -1, slice_y:Number = 0, slice_width:Number = 2, slice_height:Number = 6, slice_ellipseWidth:Number = 12, slice_ellipseHeight:Number = 12,color:Number=6710886)
		{
			this.slices = slices;
			this.radius = radiurs;
			this.slice_x = slice_x;
			this.slice_y = slice_y;
			this.slice_width = slice_width;
			this.slice_height = slice_height;
			this.slice_ellipseWidth = slice_ellipseWidth;
			this.slice_ellipseHeight = slice_ellipseHeight;
			this.slice_color = color;
			this.draw();
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event:Event) : void
		{
			removeEventListener(Event.ADDED_TO_STAGE, this.onAddedToStage);
			addEventListener(Event.REMOVED_FROM_STAGE, this.onRemovedFromStage);
			this.timer = new Timer(65);
			this.timer.addEventListener(TimerEvent.TIMER, this.onTimer, false, 0, true);
			this.timer.start();
			return;
		}// end function
		
		private function onRemovedFromStage(event:Event) : void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, this.onRemovedFromStage);
			
			this.timer.stop();
			this.timer.removeEventListener(TimerEvent.TIMER, this.onTimer);
			this.timer = null;
			return;
		}// end function
		
		private function onTimer(event:TimerEvent) : void
		{
			rotation = (rotation + 360 / this.slices) % 360;
			
			return;
		}// end function
		
		private function draw():void
		{
			var shape:Shape = null;
			var degree:Number = NaN;
			var num:int= this.slices;
			var po_degree:* = 360 / this.slices;
			while (num--)
			{
				
				shape = this.getSlice();
				shape.alpha = Math.max(0.2, 1 - 0.1 * num);
				degree = po_degree * num * Math.PI / 180;
				shape.rotation = (-po_degree) * num;
				shape.x = Math.sin(degree) * this.radius;
				shape.y = Math.cos(degree) * this.radius;
				addChild(shape);
			}
			return;
		} // end function
		
		private function getSlice():Shape
		{
			var shape:* = new Shape();
			shape.graphics.beginFill(this.slice_color);
			shape.graphics.drawRoundRect(this.slice_x,this.slice_y, this.slice_width, this.slice_height, this.slice_ellipseWidth, this.slice_ellipseHeight);
			shape.graphics.endFill();
			return shape;
		} // end function
	}
}
package k3.display.ui
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import k3.event.KEvent;
	import k3.global.Time;
	
	public class PageControl_IOS_style extends Sprite
	{
		public static const PAGECHANGE:String="pageChange";
		protected var parentWidth:Number;
		protected var parentHeight:Number;
		protected var pageTotal:uint;
		protected var curNum:int=0;
		protected var balls:Vector.<PageControlCircle>;
		protected var align:uint=0; //0:下中 1:右 2:左 3:上
		protected var ballSize:Number=5;
		protected var ballNormalColor:Number=0xffffff;
		protected var ballChosedColor:Number=0;
		protected var lineColor:Number;
		protected var normalAlpha:Number;
		protected var chosedAlpha:Number;
		protected var gap:Number=20;
		public function PageControl_IOS_style()
		{
			super();
		}
		
		/**
		 * 
		 * @param total			总数
		 * @param _parentW		场景宽
		 * @param _parentH		场景高
		 * @param _alinMode		0:下中 1:右 2:左 3:上
		 * @param _size			球直径
		 * @param normalColor	普通颜色
		 * @param chosedColor	被选择颜色
		 * 
		 */
		public function init(total:uint,_parentW:Number,_parentH:Number,_alinMode:uint=0,_size:Number=10,normalColor:Number=0xffffff,_normalAlpha:Number=1,chosedColor:Number=0,_chosedAlpha:Number=1,lineColor:Number=0xffffff):void
		{
			align=_alinMode;
			pageTotal=total;
			parentWidth=_parentW;
			parentHeight=_parentH;
			ballSize=_size;
			ballNormalColor=normalColor;
			ballChosedColor=chosedColor;
			normalAlpha=_normalAlpha;
			chosedAlpha=_chosedAlpha;
			this.lineColor=lineColor;
			initBalls();
		}
		
		protected function makeBalls():void
		{
			var curWidth:Number;
			var curHeight:Number;
			
			if(align==0)
			{
				curWidth=pageTotal*ballSize+gap*(pageTotal-1);
				curHeight=ballSize;
				this.x=parentWidth/2-curWidth/2;
				this.y=parentHeight-gap-curHeight;
				
				makeBallH();
			}
			else if(align==3)
			{
				curWidth=pageTotal*ballSize+gap*(pageTotal-1);
				curHeight=ballSize;
				this.x=parentWidth/2-curWidth/2;
				
				makeBallH();
			}
			else
			{
				curWidth=ballSize;
				curHeight=pageTotal*ballSize+gap*(pageTotal-1);
				
				if(align==1)
				{
					this.x=parentWidth-curWidth-gap;
					this.y=parentHeight/2-curHeight/2;
				}
				else
				{
					this.x=gap;
					this.y=parentHeight/2-curHeight/2;
				}
				makeBallV();
			}
		}
		
		protected function initBalls():void
		{
			balls=new Vector.<PageControlCircle>();
			var ball:PageControlCircle;
			for(var i:uint=0;i<pageTotal;i++)
			{
				ball=new PageControlCircle();
				this.addChild(ball);
				ball.init(ballSize,ballNormalColor,normalAlpha,ballChosedColor,chosedAlpha,lineColor);
				ball.x=i*(ballSize+gap);
				balls.push(ball);
				ball.name=i.toString();
				ball.addEventListener(MouseEvent.CLICK,handleBallClick);
			}
			balls[curNum].chosed();
			makeBalls();
		}
		
		protected function makeBallH():void
		{
			var ball:PageControlCircle;
			for(var i:uint=0;i<pageTotal;i++)
			{
				ball=balls[i];
				ball.x=i*(ballSize+gap);
			}
		}
		
		protected function makeBallV():void
		{
			var ball:PageControlCircle;
			for(var i:uint=0;i<pageTotal;i++)
			{	ball=balls[i];
				ball.y=i*(ballSize+gap);
				
			}
		}
		
		protected function handleBallClick(e:MouseEvent):void
		{
			
			var ball:PageControlCircle=PageControlCircle(e.currentTarget);
			trace(Time.hour_minute_second+":ball  "+ball.name+" 点击了");
			var num:uint=uint(ball.name);
			if(num==curNum)return;
			balls[curNum].normal();
			
			curNum=num;
			balls[curNum].chosed();
			this.dispatchEvent(new KEvent(PageControl_IOS_style.PAGECHANGE,num));
		}
		
		public function changePage(num:uint):void
		{
			if(num==curNum)return;
			balls[curNum].normal();
			
			curNum=num;
			balls[curNum].chosed();
		}
		
		protected function removeBallEvent():void
		{
			if(balls)
			{
				var ball:PageControlCircle;
				for(var i:uint=0;i<pageTotal;i++)
				{
					ball=balls[i];
					ball.removeEventListener(MouseEvent.CLICK,handleBallClick);
				}
				balls=null;
			}
		}
		
		public function gc():void
		{
			removeBallEvent();
		}
	}
}
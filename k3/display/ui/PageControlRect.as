package k3.display.ui
{
	import flash.display.Sprite;
	
	public class PageControlRect extends Sprite
	{
		private var normalColor:Number;
		private var chosedColor:Number;
		private var normalAlpha:Number;
		private var chosedAlpha:Number;
		private var state:uint=0;
		private var drawWidth:Number;
		private var drawHeight:Number;
		public function PageControlRect()
		{
			super();
			this.buttonMode=true;
		}
		
		
		public function drawSize(_w:Number,_h:Number):void
		{
			drawWidth=_w;
			drawHeight=_h;
			refresh();
		}
		public function setNormalColor(num:Number,a:Number):void
		{
			normalColor=num;
			normalAlpha=a;
			refresh();
		}
		
		public function setChosedColor(num:Number,a:Number):void
		{
			chosedColor=num;
			chosedAlpha=a;
			refresh();
		}
		
		public function init(_w:Number,_h:Number,_normal:Number,_normalAlpha:Number,_chosed:Number,_chosedAlpha:Number):void
		{
			drawSize(_w,_h);
			setNormalColor(_normal,_normalAlpha);
			setChosedColor(_chosed,_chosedAlpha);
			normal();
		}
		
		public function refresh():void
		{
			this.graphics.clear();
			if(state==0)
			{
				this.graphics.beginFill(normalColor,normalAlpha);
				this.mouseEnabled=true;
			}
			else
			{
				this.graphics.beginFill(chosedColor,chosedAlpha);
				this.mouseEnabled=false;
			}
			this.graphics.drawRect(0,0,drawWidth,drawHeight);
			
			this.graphics.endFill();
			
			
			
		}
		public function normal():void
		{
			state=0;
			refresh();
			
		}
		public function chosed():void
		{
			state=1;
			refresh();
		}
		
	}
}
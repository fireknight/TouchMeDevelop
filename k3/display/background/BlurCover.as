package k3.display.background
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.BlurFilter;
	import flash.filters.GlowFilter;
	import flash.geom.Matrix;
	import flash.geom.Point;
	
	import k3.display.pic.KBitmap;
	public final class BlurCover extends KBitmap
	{
		private var parentMovie:DisplayObject;
		private var coverSquare:Sprite;
		
		private var stageW:Number; 
		private var stageH:Number;
		public function BlurCover(parent:DisplayObject,w:Number = 500, h:Number = 400,color:Number=0xffffff,_alpha:Number=0.05) 
		{
			parentMovie = parent;
			stageW = w;
			stageH = h;
			super();
			
			coverSquare = new Sprite();
			coverSquare.graphics.beginFill(color,_alpha);
			coverSquare.graphics.drawRect(0,0,w,h);
			coverSquare.graphics.endFill();
			addChild(coverSquare);
			
			this.addEventListener(Event.ENTER_FRAME,initCover);
			showCover();
		}
		
		
		
		
		
		
		public function hideCover():void
		{
			//if(e.stageX < stageW && e.stageX > 0 && e.stageY < stageH && e.stageY > 0){
			this.visible = false;
			//}
			//this.dispatchEvent( new Event(Event.DEACTIVATE) );
		}
		private function showCover(e:Event = null):void
		{
			if(!this.visible){
				refresh();
			}
			// Turn on cover
			this.visible = true;
			//this.dispatchEvent( new Event(Event.ACTIVATE) );
		}
		private function initCover(e:Event):void
		{
			this.removeEventListener(Event.ENTER_FRAME,initCover);
			refresh();
			// Turn on cover
			this.visible = true;			
		}
		public function refresh():void
		{
			var bits:BitmapData = new BitmapData(stageW,stageH,false,0xffffff);
			bits.draw(coverSquare);
			bits.draw(parentMovie);
			var backgroundImage:Bitmap = new Bitmap(bits,"auto",true);
			backgroundImage.filters = new Array( new BlurFilter(6,6,4) );
			
			// Copy the blurred text and add the text
			var textOverlay:BitmapData = new BitmapData(stageW,stageW,false,0xffffff);
			textOverlay.draw(bits);
			textOverlay.draw(backgroundImage,null, null, BlendMode.LIGHTEN);
			
			textOverlay.applyFilter(textOverlay, textOverlay.rect, new Point(), new BlurFilter(5,5));
			textOverlay.draw(textOverlay);
			this.addBitmap(new Bitmap(textOverlay));
			this.mouseEnabled=true;
		}
		
	}
}
package k3.display.background
{
	import k3.display.pic.KBitmap;
	import flash.display.DisplayObject;
	public class BlackCover extends KBitmap
	{
		public function BlackCover(w:Number = 1920, h:Number = 1080,color:Number=0,_alpha:Number=0.5) 
		{
			super();
			graphics.beginFill(color,_alpha);
			graphics.drawRect(0,0,w,h);
			graphics.endFill();
			this.mouseEnabled=true;
		}
	}
}
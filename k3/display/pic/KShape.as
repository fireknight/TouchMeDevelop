package k3.display.pic
{
	import flash.display.Shape;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	
	import k3.event.KEvent;
	import k3.global.Time;
	public class KShape extends Shape
	{
		public static const IOERROR:String="ioerror";
		public static const LOADED:String="loaded";
		protected var myLoader:Loader;
		protected var url:String;
		protected var sourceBitmap:Bitmap;
		protected var startPosition:String="tl";
		protected var fileUrlLoader:FileStream;
		protected var byteWidth:uint;
		protected var byteHeight:uint;
		public function KShape()
		{
			super();
			trace("KShape 最后更新:2019/05/16");
		}
		public function getSource():Bitmap
		{
			return sourceBitmap;
		}
		
		override	public function get width():Number
		{
			return sourceBitmap.width;
		}
		
		override public function get height():Number
		{
			return sourceBitmap.height;
		}
		
		
		protected function changeSourceBitmapPosition(source:Bitmap):void
		{
			switch(startPosition)
			{
				case "tl":
					source.x=0;
					source.y=0;
					break;
				case "tr":
					source.x=-source.width;
					source.y=0;
					break;
				case "c":
					source.x=-source.width/2;
					source.y=-source.height/2;
					break;
				case "bl":
					source.x=0;
					source.y=-source.height;
					break;
				case "br":
					source.x=-source.width;
					source.y=-source.height;
					break;
			}
		}
		
		/**
		 * 通过路径添加图片
		 * @param _url 
		 * 
		 */
		public function loadPic(_url:String):void
		{
			url=_url;
			myLoader = new Loader();
			myLoader.load(new URLRequest(_url));
			
			myLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, handlePicLoaded);
			myLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, handleIOERROR);
		}
		
		protected function handleIOERROR(e:IOErrorEvent):void
		{
			this.dispatchEvent(new KEvent(KBitmap.IOERROR,url));
			trace(Time.hour_minute_second+": "+url+" 未找到文件! ");
			myLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, handlePicLoaded);
			myLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, handleIOERROR);
		}
		
		protected function handlePicLoaded(e:Event):void
		{
			var bm:Bitmap = e.target.content as Bitmap;
			bm.smoothing = true;
			sourceBitmap=bm;
			
			this.dispatchEvent(new KEvent(KBitmap.LOADED,url));
			trace(Time.hour_minute_second+": "+url+" 加载成功! ");
			myLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, handlePicLoaded);
			myLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, handleIOERROR);
			myLoader=null;
			
			addBitmap(bm);
		}
		
		/**
		 * 添加Bitmap对象定义图片
		 * @param source 
		 * 
		 */
		public function addBitmap(source:Bitmap):void
		{
			sourceBitmap=source;
			this.graphics.beginBitmapFill(source.bitmapData);
			this.graphics.drawRect(0,0,source.width,source.height);
			this.graphics.endFill();
		}
		
		/**
		 *
		 * 通过二进制文件路径添加图片
		 * @param _url
		 * @param _width
		 * @param _height
		 * 
		 */
		public function addBitmapByByteArray(_url:String,_width:uint,_height:uint):void
		{
			url=_url;
			byteWidth=_width;
			byteHeight=_height;
			var file:File=new File(File.applicationDirectory.resolvePath(_url).nativePath);
			if(!file.exists)
			{
				this.dispatchEvent(new KEvent(KBitmap.IOERROR,url));
				trace(Time.hour_minute_second+": "+url+" 未找到文件! ");
				return;
				
			}
			else if(file.isDirectory)
			{
				this.dispatchEvent(new KEvent(KBitmap.IOERROR,url));
				trace(Time.hour_minute_second+": "+url+" 是个文件夹! ");
				return;
			}
			fileUrlLoader=new FileStream();
			fileUrlLoader.addEventListener(Event.COMPLETE,hadnlePreFileLoaded);
			
			fileUrlLoader.openAsync(file,FileMode.READ); 
		}
		
		protected function hadnlePreFileLoaded(e:Event):void
		{
			
			var bmd1:BitmapData = new BitmapData(byteWidth,byteHeight,true,0x00ffffff);
			var byteArray:ByteArray=new ByteArray();
			fileUrlLoader.readBytes(byteArray, 0, fileUrlLoader.bytesAvailable);
			byteArray.uncompress();
			fileUrlLoader.position=0;
			fileUrlLoader.close();
			fileUrlLoader.removeEventListener(Event.COMPLETE,hadnlePreFileLoaded);
			fileUrlLoader=null;
			bmd1.lock();
			bmd1.setPixels(new Rectangle(0,0,byteWidth,byteHeight),byteArray);
			bmd1.unlock();
			sourceBitmap=new Bitmap(bmd1,"auto",true);
			this.dispatchEvent(new KEvent(KBitmap.LOADED,url));
			trace(Time.hour_minute_second+": "+url+" 加载成功! ");
			addBitmap(sourceBitmap);
		}
		
		
		/**
		 * 
		 *  清除位图
		 */
		public function clear():void
		{
			if(sourceBitmap)
			{
				this.graphics.clear();
				
				sourceBitmap.bitmapData.dispose();
				sourceBitmap=null;
			}
		}
		
		
		/**
		 *  克隆对象
		 * @return 返回KBitmap
		 * 
		 */
		public function clone():KBitmap
		{
			
			if(sourceBitmap)
			{
				var kb:KBitmap=new KBitmap();
				var myBitmapData:BitmapData = new BitmapData(sourceBitmap.width,sourceBitmap.height,true,0x00000000);
				myBitmapData.draw(sourceBitmap);
				
				var myBitMap:Bitmap = new Bitmap(myBitmapData,"auto",true);
				myBitMap.smoothing=true;
				
				kb.addBitmap(myBitMap);
				kb.changeStartPoint(startPosition);
				return kb;
			}
			else
			{
				return null;
			}
			
		}
		
		
		/**
		 * 
		 *适应全屏 
		 */
		public function fitToFullScreen():void
		{
			if(stage)
			{
				var rat:Number=sourceBitmap.width/sourceBitmap.height;
				sourceBitmap.width=stage.stageWidth;
				sourceBitmap.height=sourceBitmap.width/rat;
				if(sourceBitmap.height<stage.stageHeight)
				{
					sourceBitmap.height=stage.stageHeight;
					sourceBitmap.width=sourceBitmap.height*rat;
				}
				this.x=stage.stageWidth/2-sourceBitmap.width/2;
				this.y=stage.stageHeight/2-sourceBitmap.height/2;
			}
		}
		
		
		/**
		 * 适应尺寸 
		 * @param targetWidth
		 * @param targetHeight
		 * 
		 */
		public function fitSize(targetWidth:uint,targetHeight:uint):void
		{
			var rat:Number=sourceBitmap.width/sourceBitmap.height;
			sourceBitmap.width=targetWidth;
			sourceBitmap.height=sourceBitmap.width/rat;
			if(sourceBitmap.height<targetHeight)
			{
				sourceBitmap.height=targetHeight;
				sourceBitmap.width=sourceBitmap.height*rat;
			}
		}
		
		
		/**
		 * 释放内存 
		 * 
		 */
		public function gc():void
		{
			clear();
		}
	}
}
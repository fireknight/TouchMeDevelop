package k3.display.board
{
	import flash.display.InteractiveObject;

	public class KBoardButtonObject extends Object
	{
		private var _uitarget:InteractiveObject;
		private var _uieventName:String;
		public function KBoardButtonObject()
		{
			super();
		}

		/**
		 *	点击事件名称 
		 * @return 
		 * 
		 */
		public function get uieventName():String
		{
			return _uieventName;
		}
		/**
		 *	点击事件名称 
		 * @return 
		 * 
		 */
		public function set uieventName(value:String):void
		{
			_uieventName = value;
		}

		public function get uitarget():InteractiveObject
		{
			return _uitarget;
		}

		public function set uitarget(value:InteractiveObject):void
		{
			_uitarget = value;
		}

	}
}
package k3.display.board
{
	import flash.display.InteractiveObject;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import k3.global.Time;
	import k3.event.KEvent;
	
	public class KBoard extends Sprite
	{
		protected var boardSprite:Sprite;
		protected var boardButtons:Vector.<KBoardButtonObject>;
		public function KBoard()
		{
			super();
		}
		
		
		public function drawBg(w:Number,h:Number,color:Number=0,alpha:Number=0.5):void
		{
			this.graphics.clear();
			this.graphics.beginFill(color,alpha);
			this.graphics.drawRect(0,0,w,h);
			this.graphics.endFill();
		}
		
		/**
		 * 
		 * @param mc
		 * @param position "tl"左上 "c"居中 "tr"右上 "br"右下 "bl"左下
		 * @param scaleX
		 * @param scaleY
		 * 
		 */
		public function createBoardWithSpriteAndPosition(mc:Sprite,position:String="tl",scaleX:Number=1,scaleY:Number=1):void
		{
			boardSprite=mc;
			boardSprite.scaleX=scaleX;
			boardSprite.scaleY=scaleY;
			switch(position)
			{
				case "tl":
					boardSprite.x=0;
					boardSprite.y=0;
					break;
				case "tr":
					boardSprite.x=this.width-boardSprite.width;
					boardSprite.y=0;
					break;
				case "c":
					boardSprite.x=this.width/2-boardSprite.width/2;
					boardSprite.y=this.height/2-boardSprite.height/2;
					break;
				case "bl":
					boardSprite.x=0;
					boardSprite.y=this.height-boardSprite.height;
					break;
				case "br":
					boardSprite.x=this.width-boardSprite.width;
					boardSprite.y=this.height-boardSprite.height;
					break;
			}
			this.addChild(boardSprite);
		}
		
		
		
		/**
		 * 
		 * @param mc
		 * @param _x
		 * @param _y
		 * @param scaleX
		 * @param scaleY
		 * 
		 */
		public function createBoardWithSprite(mc:Sprite,_x:Number=0,_y:Number=0,scaleX:Number=1,scaleY:Number=1):void
		{
			boardSprite=mc;
			boardSprite.scaleX=scaleX;
			boardSprite.scaleY=scaleY;
			boardSprite.x=_x;
			boardSprite.y=_y;
			this.addChild(boardSprite);
		}
		
		
		/**
		 * 
		 * @param bt	SimpleButton对象
		 * @param eventName   点击事件名称
		 * 
		 */
		public function addBoardButtonWithSimpleButton(bt:SimpleButton,eventName:String):void
		{
			if(!boardButtons)
			{
				boardButtons=new Vector.<KBoardButtonObject>();
			}
			var kbo:KBoardButtonObject=new KBoardButtonObject();
			kbo.uitarget=bt;
			bt.addEventListener(MouseEvent.CLICK,handleBoardBtClick);
			kbo.uieventName=eventName;
			boardButtons.push(kbo);
		}
		
		
		protected function handleBoardBtClick(e:MouseEvent):void
		{
			var bt:InteractiveObject=InteractiveObject(e.currentTarget);
			var eventName:String=findBtEventName(bt);
			trace(Time.hour_minute_second+": "+bt.name+" 点击了并发送 "+eventName+" 事件");
			this.dispatchEvent(new KEvent(eventName));
		}
		
		protected function findBtEventName(target:InteractiveObject):String
		{
			if(boardButtons)
			{
				var length:uint=boardButtons.length;
				var kbo:KBoardButtonObject;
				var bt:InteractiveObject;
				for(var i:uint=0;i<length;i++)
				{
					kbo=boardButtons[i];
					if(target.name==kbo.uitarget.name)
					{
						return kbo.uieventName;
					}
				}
			}
			return "";
		}
		
		protected function removeBts():void
		{
			if(boardButtons)
			{
				var length:uint=boardButtons.length;
				var kbo:KBoardButtonObject;
				var bt:InteractiveObject;
				for(var i:uint=0;i<length;i++)
				{
					kbo=boardButtons[i];
					bt=kbo.uitarget;
					bt.removeEventListener(MouseEvent.CLICK,handleBoardBtClick);
				}
				boardButtons=null;
			}
		}
		
		public function gc():void
		{
			removeBts();
		}
	}
}
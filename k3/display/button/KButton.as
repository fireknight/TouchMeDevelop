package k3.display.button
{
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	
	import k3.display.pic.KBitmap;
	import k3.event.KEvent;
	import k3.global.Time;
	import k3.utils.BitMapMethod;
	
	public class KButton extends Sprite
	{
		public static const LOADED:String="loaded";
		protected var normalSprite:Sprite;
		protected var pressedSprite:Sprite;
		protected var chosedSprite:Sprite;
		protected var chosedStopFrames:Array=new Array();
		protected var pressedStopFrames:Array=new Array();
		protected var haveMovieClip:Boolean=false;
		protected var normalBitmap:Bitmap;
		protected var pressIsNormal:Boolean=false;
		protected var chosedIsNormal:Boolean=false;
		private var chosed:Boolean=false;
		private var coverArea:Sprite;
		public function KButton()
		{
			super();
		}
		
		/**
		 * 加载普通状态图片文件 
		 * @param url
		 * 
		 */
		public function loadNormalFile(url:String):void
		{
			normalSprite=new Sprite();
			this.addChild(normalSprite);
			var kb:KBitmap=new KBitmap();
			kb.loadPic(url);
			normalSprite.addChild(kb);
			kb.mouseEnabled=true;
			kb.addEventListener(KBitmap.LOADED,handleNormalComplete,false,0,true);
		}
		
		/**
		 * 加载普通状态位图对象 
		 * @param url           
		 * 
		 */
		public function addBitmap(source:Bitmap):void
		{
			normalSprite=new Sprite();
			this.addChild(normalSprite);
			var kb:KBitmap=new KBitmap();
			kb.addBitmap(source);
			normalSprite.addChild(kb);
			kb.mouseEnabled=true;
		}
		
		protected function handleNormalComplete(e:KEvent):void
		{
			var kb:KBitmap=normalSprite.getChildAt(0) as KBitmap;
			if(kb)
			{
				normalBitmap=kb.getSource();
				if(pressIsNormal&&!pressedSprite)
				{
					copyNormalBitmapToPressed();
				}
				if(chosedIsNormal&&!chosedSprite)
				{
					copyNormalBitmapToChosed();
				}
				
				coverArea=new Sprite();
				this.addChild(coverArea);
				coverArea.graphics.beginFill(0,0);
				coverArea.graphics.drawRect(0,0,kb.width,kb.height);
				coverArea.graphics.endFill();
			}
			this.dispatchEvent(new KEvent(LOADED));
		}
		/**
		 * 
		 * 复制普通状态图片到按下状态
		 * 
		 */
		public function copyNormalBitmapToPressed():void
		{
			pressIsNormal=true;
			if(normalBitmap)
			{
				var  bmp:Bitmap=BitMapMethod.copy(normalBitmap);
				pressedSprite=new Sprite();
				this.addChild(pressedSprite);
				pressedSprite.addChild(bmp);
				bmp=copyNormalBitmap();
				bmp.alpha=0.5;
				pressedSprite.addChild(bmp);
				pressedSprite.visible=false;
			}
			
			
		}
		/**
		 * 
		 * 复制普通状态图片到被选择状态
		 * 
		 */
		public function copyNormalBitmapToChosed():void
		{
			chosedIsNormal=true;
			if(normalBitmap)
			{
				var  bmp:Bitmap=BitMapMethod.copy(normalBitmap);
				chosedSprite=new Sprite();
				this.addChild(chosedSprite);
				chosedSprite.addChild(bmp);
				bmp=copyNormalBitmap();
				bmp.alpha=0.5;
				chosedSprite.addChild(bmp);
				chosedSprite.visible=false;
			}
		}
		
		protected function copyNormalBitmap():Bitmap
		{
			if(normalBitmap)
			{
				var  bmp:Bitmap=BitMapMethod.copy(normalBitmap);
				var bmpd:BitmapData=bmp.bitmapData;
				for(var i:uint = 0; i < bmpd.width; i ++)
				{
					for(var j:uint = 0; j < bmpd.height; j ++)
					{
						
						if(BitMapMethod.getColorAlpha(bmp,i,j)>0)
						{
							bmpd.setPixel(i, j, 0)
							//							bmpd.setPixel32(i, j, 0x00000000ff);
						}
					}
				}
				return  new Bitmap(bmpd);
			}
			trace(Time.hour_minute_second+": "+"错误!一般状态的图片未加载");
			return null;
		}
		
		/**
		 * 加载按下状态的图片 
		 * @param url
		 * 
		 */
		public function loadPressedFile(url:String):void
		{
			pressedSprite=new Sprite();
			this.addChildAt(pressedSprite,0);
			var kb:KBitmap=new KBitmap();
			kb.loadPic(url);
			pressedSprite.addChild(kb);
			pressedSprite.visible=false;
		}
		
		/**
		 * 加载被选择状态的图片 
		 * @param url
		 * 
		 */
		public function loadChosedFile(url:String):void
		{
			chosedSprite=new Sprite();
			this.addChildAt(chosedSprite,0);
			var kb:KBitmap=new KBitmap();
			kb.loadPic(url);
			chosedSprite.addChild(kb);
			chosedSprite.visible=false;
		}
		
		protected var pressedMovieClip:MovieClip;
		/**
		 * 加载按下状态的图片 
		 * @param url
		 * 
		 */
		public function addPressedMovieClip(mc:MovieClip,_stopFrames:Array):void
		{
			pressedSprite=new Sprite();
			this.addChild(pressedSprite);
			pressedSprite.addChild(mc);
			pressedMovieClip=mc;
			pressedStopFrames=_stopFrames;
			mc.gotoAndStop(1);
			pressedSprite.visible=false;
		}
		
		private function listenPressedMovieClip(e:Event):void
		{
			for(var i:uint=0;i<pressedStopFrames.length;i++)
			{
				if(pressedMovieClip.currentFrame==pressedStopFrames[i])
				{
					pressedMovieClip.gotoAndStop(pressedStopFrames[i]);
					pressedMovieClip.removeEventListener(Event.ENTER_FRAME,listenPressedMovieClip);
					break;
				}
			}
			
		}
		
		protected var chosedMovieClip:MovieClip;
		/**
		 * 加载被选择状态的图片 
		 * @param url
		 * 
		 */
		public function addChosedMovieClip(mc:MovieClip,_stopFrames:Array):void
		{
			chosedSprite=new Sprite();
			this.addChild(chosedSprite);
			chosedSprite.addChild(mc);
			chosedMovieClip=mc;
			chosedStopFrames=_stopFrames;
			mc.gotoAndStop(1);
			chosedSprite.visible=false;
			
		}
		private function listenChosedMovieClip(e:Event):void
		{
			for(var i:uint=0;i<chosedStopFrames.length;i++)
			{
				if(pressedMovieClip.currentFrame==chosedStopFrames[i])
				{
					chosedMovieClip.gotoAndStop(chosedStopFrames[i]);
					chosedMovieClip.removeEventListener(Event.ENTER_FRAME,listenChosedMovieClip);
					break;
				}
			}
		}
		
		public function getChosedMovieClip():MovieClip
		{
			return chosedMovieClip;
		}
		public function getPressedMovieClip():MovieClip
		{
			return pressedMovieClip;
		}
		
		/**
		 * 
		 * 激活本按钮鼠标事件
		 */
		public function init():void
		{
			
			this.addEventListener(MouseEvent.MOUSE_DOWN,handleMouseDown,false,0,true);
		}
		
		
		protected function handleMouseDown(e:MouseEvent):void
		{
			if(chosed)return;
			normalSprite.visible=false;
			pressedSprite.visible=true;
			chosedSprite.visible=false;
			if(pressedMovieClip)
			{
				pressedMovieClip.addEventListener(Event.ENTER_FRAME,listenPressedMovieClip);
				pressedMovieClip.play();
			}
			
			
			this.addEventListener(MouseEvent.MOUSE_OUT,handleMouseUp);
			this.addEventListener(MouseEvent.MOUSE_UP,handleMouseUp);
		}
		
		protected function handleMouseUp(e:MouseEvent):void
		{
			normalSprite.visible=true;
			pressedSprite.visible=false;
			chosedSprite.visible=false;
			if(pressedMovieClip)
			{
				if(pressedMovieClip.hasEventListener(Event.ENTER_FRAME))
				{
					pressedMovieClip.removeEventListener(Event.ENTER_FRAME,listenPressedMovieClip);
					
				}
				pressedMovieClip.gotoAndStop(1);
			}
			this.removeEventListener(MouseEvent.MOUSE_OUT,handleMouseUp);
			this.removeEventListener(MouseEvent.MOUSE_UP,handleMouseUp);
		}
		
		
		
		/**
		 * 把按钮设置为被选择状态 
		 * 
		 */
		public function handleChosed():void
		{
			chosed=true;
			normalSprite.visible=false;
			pressedSprite.visible=false;
			chosedSprite.visible=true;
			if(chosedMovieClip)
			{
				chosedMovieClip.addEventListener(Event.ENTER_FRAME,listenChosedMovieClip);
				chosedMovieClip.play();
			}
		}
		/**
		 * 把按钮设置为非被选择状态 
		 * 
		 */
		public function handleUnChosed():void
		{
			chosed=false;
			normalSprite.visible=true;
			pressedSprite.visible=false;
			chosedSprite.visible=false;
			if(chosedMovieClip)
			{
				if(chosedMovieClip.hasEventListener(Event.ENTER_FRAME))
				{
					chosedMovieClip.removeEventListener(Event.ENTER_FRAME,listenChosedMovieClip);
					
				}
				chosedMovieClip.gotoAndStop(1);
			}
		}
		
		/**
		 * 当前按钮是否被选择
		 * @return 
		 * 
		 */
		public function isChosed():Boolean
		{
			return chosed;
		}
		
		private function removeNormal():void
		{
			if(normalSprite)
			{
				var kb:KBitmap=normalSprite.getChildAt(0) as KBitmap;
				var bm:Bitmap=kb.getSource();
				bm.bitmapData.dispose();
			}
		}
		
		private function removeSprite(sp:Sprite):void
		{
			var kb:KBitmap;
			var bm:Bitmap;
			var obj:*;
			for(var i:uint=0;i<sp.numChildren;i++)
			{
				obj=pressedSprite.getChildAt(i);
				if(obj is KBitmap)
				{
					kb=KBitmap(obj);
					bm=kb.getSource();
					bm.bitmapData.dispose();
				}
				else if(obj is Bitmap)
				{
					bm=Bitmap(obj);
					bm.bitmapData.dispose();
				}
			}
		}
		
		private function removePressed():void
		{
			if(pressedSprite)
			{
				removeSprite(pressedSprite);
				
			}
		}
		
		private function removeChosed():void
		{
			if(chosedSprite)
			{
				removeSprite(chosedSprite);
			}
		}
		
		/**
		 *释放此对象 
		 * 
		 */
		public function gc():void
		{
			removeNormal();
			removePressed();
			removeChosed();
			this.removeEventListener(MouseEvent.MOUSE_DOWN,handleMouseDown);
			
		}
		
	}
}
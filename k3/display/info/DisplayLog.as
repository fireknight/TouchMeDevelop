package k3.display.info
{
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.engine.ElementFormat;
	import flash.text.engine.FontDescription;
	import flash.text.engine.FontPosture;
	import flash.text.engine.FontWeight;
	import flash.text.engine.TextBlock;
	import flash.text.engine.TextElement;
	import flash.text.engine.TextLine;
	import k3.global.Time;
	public class DisplayLog extends Sprite
	{
		private var txt:TextField;
		private var lrcLines:Vector.<Sprite> ;
		private var _lineSpace:Number = 30;
		private var _lineWidth:Number=800;
		private var _maxLine:uint=20;
		private var _fontSize:uint=20;
		public function DisplayLog()
		{
			
			
			this.mouseChildren=false;
			this.mouseEnabled=false;
			lrcLines=new Vector.<Sprite>();
			
		}
		
		
		public function set fontSize(value:uint):void
		{
			_fontSize = value;
		}
		
		public function set maxLine(value:uint):void
		{
			_maxLine = value;
		}
		
		
		public function set lineWidth(value:Number):void
		{
			_lineWidth = value;
		}
		
		public function set lineSpace(value:Number):void
		{
			_lineSpace = value;
		}
		
		public function yelloTxt(str:String):void
		{
			addTxt(str,0xffff00);
		}
		
		public function greenTxt(str:String):void
		{
			addTxt(str,0x00FF00);
		}
		
		public function redTxt(str:String):void
		{
			addTxt(str,0xFF0000);
		}
		
		public function whiteTxt(str:String):void
		{
			addTxt(str,0xFFFFFF);
		}
		
		public function blackTxt(str:String):void
		{
			addTxt(str,0);
		}
		
		
		private function addTxt(str:String,color:uint):void
		{
			var index:uint = lrcLines.length;
			if(index>=_maxLine)
			{
				var arr:Array=new Array();
				for(var i:uint=0;i<index-_maxLine+1;i++)
				{
					var sp:Sprite=lrcLines[i];
					this.removeChild(sp);
					
					
				}
				lrcLines=lrcLines.slice(index-_maxLine+1);//因为在增加一个,所以至少删除一个
				index = lrcLines.length;
				
				for(i=0;i<index;i++)
				{
					var sp:Sprite=lrcLines[i];
					sp.y=_lineSpace*i;
					
					
				}
				
				
			}
			var textBlock:TextBlock = new TextBlock();
			var textLine:TextLine ;
			var fontDescriptionNormal:FontDescription = new FontDescription("Arial", FontWeight.NORMAL , FontPosture.NORMAL);
			var format:ElementFormat = new ElementFormat(fontDescriptionNormal); 
			format.color=color;
			format.fontSize = _fontSize;
			var textElement:TextElement = new TextElement(Time.hour_minute_second+" "+str, format);
			
			textLine = textBlock.createTextLine (null, _lineWidth,0,true)
			textBlock.content = textElement; 
			textLine = textBlock.createTextLine (textLine, _lineWidth,0,true);
			var sp:Sprite=new Sprite();
			sp.addChild(textLine);
			sp.y=index*_lineSpace;
			
			this.addChild(sp);
			sp.cacheAsBitmap=true;
			lrcLines.push(sp);
		}
		
	}
}
package k3.effect
{
	import flash.display.*;
	import flash.events.*;
	import flash.geom.*;
	
	import k3.display.pic.KBitmap;
	public class Reflection extends KBitmap
	{
		private var _disTarget:DisplayObject;
		private var _numStartFade:Number = .3;
		private var _numMidLoc:Number = .5;
		private var _numEndFade:Number = 0;
		private var _numSkewX:Number = 0;
		private var _numScale:Number = 1;
		private var _bmpReflect:Bitmap;
		private var _distance:Number;
		private var _offset:Number;
		private var _height:Number;
		public function Reflection(set_disTarget:DisplayObject, set_height:Number,set_numStartFade:Number, set_numMidLoc:Number, set_numEndFade:Number, set_numSkewX:Number, set_numScale:Number, distance:Number, offset:Number = 5)
		{
			super()
			_disTarget = set_disTarget;
			_height=set_height;
			_numStartFade = set_numStartFade;
			_numMidLoc = set_numMidLoc;
			_numEndFade = set_numEndFade;
			_numSkewX = set_numSkewX;
			_numScale = set_numScale;
			_distance = distance;
			_offset = offset;
			_bmpReflect = new Bitmap(new BitmapData(1, 1, true, 0));
			this.addChild(_bmpReflect);
			createReflection();
			trace(1031);
		}
		
		// Create reflection
		private function createReflection(event:Event = null):void
		{
			// Reflection
			var bmpDraw:BitmapData=new BitmapData(_disTarget.width,_height,true,0);
			var matSkew:Matrix=new Matrix(1,0,_numSkewX,-1*_numScale,0,_height);//此处必须要用_height
			var recDraw:Rectangle = new Rectangle(0, 0, _disTarget.width, _height * (2 - _numScale));
			var potSkew:Point=matSkew.transformPoint(new Point(0,_disTarget.height));//此处不能替换成_height
			matSkew.tx=potSkew.x*-1;
			matSkew.ty = (potSkew.y - _height) * -1;//此处必须要用_height
			bmpDraw.draw(_disTarget, matSkew, null, null, recDraw, true);
			
			// Fade
			var shpDraw:Shape = new Shape();
			var matGrad:Matrix = new Matrix();
			var arrAlpha:Array = new Array(_numStartFade, (_numStartFade - _numEndFade) / 2, _numEndFade);
			var arrMatrix:Array=new Array(0,0xFF*_numMidLoc,0xFF);
			matGrad.createGradientBox(_disTarget.width, _height, 0.5 * Math.PI);
			//这里设置投影高度=_height;
			shpDraw.graphics.beginGradientFill(GradientType.LINEAR, new Array(0,0,0), arrAlpha, arrMatrix, matGrad);
			shpDraw.graphics.drawRect(0, 0, _disTarget.width, _height);
			shpDraw.graphics.endFill();
			bmpDraw.draw(shpDraw, null, null, BlendMode.ALPHA);
			
			_bmpReflect.bitmapData.dispose();
			_bmpReflect.bitmapData=bmpDraw;
			
			_bmpReflect.filters=_disTarget.filters;
			
			this.x = _disTarget.x;
			this.y = (_disTarget.y + _disTarget.height) - _distance;
		}
	}
}
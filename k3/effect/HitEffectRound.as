package k3.effect
{
	import com.greensock.TweenLite;
	
	import flash.display.Shape;
	import flash.display.Sprite;

	public final class HitEffectRound extends Sprite
	{
		
		private var count:uint=0;
		public function HitEffectRound()
		{
			super();
			this.mouseChildren=false;
			this.mouseEnabled=false;
		}
		
		public function show(color:uint):void
		{
			for(var i:uint=0;i<2;i++)
			{
				var po:Shape=new Shape();
				this.addChild(po);
				po.graphics.beginFill(color,1);
				po.graphics.drawCircle(0,0,10);
				po.graphics.endFill();
				TweenLite.to(po,0.5,{alpha:0,scaleX:5,scaleY:5,onComplete:handleDisaple,delay:i*0.2});
			}
		}
		
		private function handleDisaple():void
		{
			count++;
			if(count==2)
			{
				this.parent.removeChild(this);
			}
			
		}
	}
}
 package k3.component.pic
{
	import com.greensock.TweenLite;
	import com.greensock.easing.*;
	
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	import k3.display.background.BlackCover;
	import k3.display.pic.KShape;
	import k3.display.ui.LoadingCircle0;
	import k3.event.KEvent;
	import k3.global.Time;
	
	public class PicShowBox extends Sprite
	{
		public static const BGLICK:String="bgClick";
		public static const IMAGELOADED:String="imageReady";
		protected var blackBg:BlackCover;
		protected var urls:Vector.<String>;
		protected var paretnMc:Sprite;
		protected var parentWidth:Number;
		protected var parentHeight:Number;
		protected var pageTotal:uint;
		public var curNum:int=0;
		protected var _canChange:Boolean=true;
		protected var leftBtn:DisplayObject;
		protected var rightBtn:DisplayObject;
		protected var bgShape:Shape;
		protected var bgWidth:Number;
		protected var bgHeight:Number;
		protected var titles:Vector.<String>;
		protected var des:Vector.<String>;
		protected var titleSize:Number;
		protected var desSize:Number;
		protected var titleColor:Number;
		protected var desColor:Number;
		protected var titleTxt:TextField;
		protected var desTxt:TextField;
		protected var loadingMc:LoadingCircle0;
		protected var titleAlign:String;
		protected var desAlign:String;
		protected var curBitmap:KShape;
		public function PicShowBox()
		{
			super();
//			trace(Time.hour_minute_second+": "+"PicShowBox:未完成版本,暂未加入左右箭头和加载动画");
			trace(Time.hour_minute_second+": "+"PicShowBox:未完成版本,暂未加入文本显示");
		}
		
		public function get canChange():Boolean
		{
			return _canChange;
		}


		/**
		 * 
		 * @param paths
		 * @param _parent
		 * @param _parentW
		 * @param _parentH
		 * @param leftControl		左翻按钮,定义后跟随图片调整位置,不用做控制,触发
		 * @param rightControl		右翻按钮,定义后跟随图片调整位置,不用做控制,触发
		 * @param loadingBgWidth	加载图片过渡动画背景宽度
		 * @param loadingBgHeight	加载图片过渡动画背景高度
		 * @param loadingBgColor	加载图片过渡动画背景颜色
		 * @param loadingBgAlpha	加载图片过渡动画背景透明度
		 * @param bgMode			0:无 1:黑色背景
		 * @param _titles			图片标题列表
		 * @param _des				图片文字列表
		 * @param _titleFontSize	标题字体大小
		 * @param _desFontSize		文字字体大小
		 * @param _titleColor		标题颜色
		 * @param _desColor			文字颜色
		 * 
		 */
		public function init(paths:Vector.<String>,_parent:Sprite,_parentW:Number,_parentH:Number,leftControl:DisplayObject,rightControl:DisplayObject,loadingBgWidth:Number=0,loadingBgHeight:Number=0,loadingBgColor:Number=0,loadingBgAlpha:Number=0,bgMode:uint=1,_titles:Vector.<String>=null,_des:Vector.<String>=null,titleFontSize:Number=0,desFontSize:Number=0,_titleColor:Number=0,_desColor:Number=0,_titleAlign:String="left",_desAlign:String="left"):void
		{
			urls=paths;
			paretnMc=_parent;
			parentWidth=_parentW;
			parentHeight=_parentH;
			pageTotal=urls.length;
			if(bgMode==1)
			{
				blackBg=new BlackCover(parentWidth,parentHeight);
				this.addChild(blackBg);
				blackBg.addEventListener(MouseEvent.CLICK,handleBlackBgClick);
			}
			
			if(loadingBgWidth>0)
			{
				bgShape=new Shape();
				bgShape.graphics.beginFill(loadingBgColor,loadingBgAlpha);
				bgShape.graphics.drawRect(0,0,loadingBgWidth,loadingBgHeight);
				bgWidth=loadingBgWidth;
				bgHeight=loadingBgHeight;
				bgShape.graphics.endFill();
				this.addChild(bgShape);
				bgShape.x=parentWidth/2-bgWidth/2;
				bgShape.y=parentHeight/2-bgHeight/2;
			}
			if(leftControl)
			{
				leftBtn=leftControl;
				rightBtn=rightControl;
			}
			if(!curBitmap)
			{
				curBitmap=new KShape();
				this.addChild(curBitmap);
			}
			titles=_titles;
			des=_des;
			titleSize=titleFontSize;
			desSize=desFontSize;
			titleColor=_titleColor;
			desColor=_desColor;
			titleAlign=_titleAlign;
			desAlign=_desAlign;
			showNext();
		}
		
		private function releaseLastBitmap():void
		{
			if(curBitmap)
			{
				curBitmap.gc();
			}
		}
		
		public function showNext():void
		{
			_canChange=false;
			releaseLastBitmap();
			if(bgShape)
			{
				showLoading();
			}
			else
			{
				curBitmap.alpha=0;
				curBitmap.addEventListener(KShape.LOADED,handleBitmapReady);
 				curBitmap.loadPic(urls[curNum]);
			}
		}
		
		private function handleBitmapReady(e:KEvent):void
		{
			trace(Time.hour_minute_second+":"+urls[curNum]+ "加载了");
			hideLoading();
			TweenLite.to(curBitmap,0.5,{alpha:1,delay:0.5});
			curBitmap.x=parentWidth/2-curBitmap.width/2;
			curBitmap.y=parentHeight/2-curBitmap.height/2;
			moveLeftRight();
			this.dispatchEvent(new KEvent("imageReady",curNum));
			_canChange=true;
		}
		
		private function moveLeftRight():void
		{
			if(leftBtn)
			{
				var leftPosition_x:Number=parentWidth/2-curBitmap.width/2-50-leftBtn.width;
				var leftPosition_y:Number=parentHeight/2-leftBtn.height/2;
				TweenLite.to(leftBtn,0.5,{x:leftPosition_x,y:leftPosition_y});
				var rightPosition_x:Number=parentWidth/2+curBitmap.width/2+50;
				var rightPosition_y:Number=parentHeight/2-leftBtn.height/2;
				TweenLite.to(rightBtn,0.5,{x:rightPosition_x,y:rightPosition_y});
			}
		}
		
		private function hideLoading():void
		{
			if(bgShape)
			{
				if(loadingMc)
				{
					this.removeChild(loadingMc);
					loadingMc=null;
				}
				TweenLite.to(bgShape,0.5,{width:curBitmap.width,height:curBitmap.height,x:parentWidth/2-curBitmap.width/2,y:parentHeight/2-curBitmap.height/2});
			}
		}
		
		
		private function showLoading():void
		{
			curBitmap.alpha=0;
			TweenLite.to(bgShape,0.5,{width:bgWidth,height:bgHeight,onComplete:startLoadCurBitmap,x:parentWidth/2-bgWidth/2,y:parentHeight/2-bgHeight/2});
			loadingMc=new LoadingCircle0();
			this.addChild(loadingMc);
			loadingMc.x=parentWidth/2-loadingMc.width/2;
			loadingMc.y=parentHeight/2-loadingMc.height/2;
			
		}
				
		private function startLoadCurBitmap():void
		{
			curBitmap.addEventListener(KShape.LOADED,handleBitmapReady);
			curBitmap.loadPic(urls[curNum]);
		}
		
		private function handleBlackBgClick(e:MouseEvent):void
		{
			trace(Time.hour_minute_second+": bgClick  点击了");
			this.dispatchEvent(new KEvent("bgClick"));
		}
		
		public function gc():void
		{
			if(blackBg)
			{
				blackBg.removeEventListener(MouseEvent.CLICK,handleBlackBgClick);
				this.removeChild(blackBg);
				blackBg=null;
			}
			if(curBitmap)
			{
				curBitmap.gc();
				curBitmap.parent.removeChild(curBitmap);
				curBitmap=null;
			}
		}
		
	}
}
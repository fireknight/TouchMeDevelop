package k3.component.pic
{
	import com.greensock.TweenLite;
	import com.greensock.easing.*;
	
	import flash.display.Sprite;
	import flash.utils.setTimeout;
	
	import k3.control.TouchMove;
	import k3.display.background.BlackCover;
	import k3.display.background.BlurCover;
	import k3.display.pic.KBitmap;
	import k3.display.ui.PageControl_IOS_style;
	import k3.event.KEvent;
	import k3.global.Time;
	
	public class PicShowWithPageControl extends Sprite
	{
		public static const PAGECHANGE:String="pageChanged";
		protected var blurBg:BlurCover;
		protected var blackBg:BlackCover;
		protected var urls:Vector.<String>;
		protected var paretnMc:Sprite;
		protected var parentWidth:Number;
		protected var parentHeight:Number;
		protected var pageControl:PageControl_IOS_style;
		protected var pics:Vector.<KBitmap>
		protected var pageTotal:uint;
		protected var curNum:int=0;
		protected var tm:TouchMove;
		protected var canChange:Boolean=true;
		protected var dir:String;
		protected var lastKb:KBitmap;
		public function PicShowWithPageControl()
		{
			super();
		}
		
		/**
		 * 
		 * @param paths
		 * @param _parentW			场景宽
		 * @param _parentH			场景高
		 * @param bgMode			0:无 1:模糊背景 2:黑色背景
		 * @param _alinMode			0:下中 1:右 2:左 3:上
		 * @param _size				球直径
		 * @param normalColor		普通颜色
		 * @param chosedColor		被选择颜色
		 * 
		 */
		public function init(paths:Vector.<String>,_parent:Sprite,_parentW:Number,_parentH:Number,bgMode:uint=1,_haveControl:Boolean=true,_alinMode:uint=0,_size:Number=10,normalColor:Number=0xffffff,chosedColor:Number=0):void
		{
			//trace("PicShowWithPageControl 最后更新:2019/04/11");
			trace("PicShowWithPageControl 最后更新:2019/05/15");
			urls=paths;
			paretnMc=_parent;
			parentWidth=_parentW;
			parentHeight=_parentH;
			pageTotal=urls.length;
			if(bgMode==1)
			{
				blurBg=new BlurCover(paretnMc,parentWidth,parentHeight);
				this.addChild(blurBg);
			}
			else if(bgMode==2)
			{
				blackBg=new BlackCover(parentWidth,parentHeight);
				this.addChild(blackBg);
			}
			if(_haveControl)
			{
				pageControl=new PageControl_IOS_style();
				
				this.addChild(pageControl);
			
				pageControl.init(paths.length,_parentW,_parentH,_alinMode,_size,normalColor,1,chosedColor);
				pageControl.addEventListener(PageControl_IOS_style.PAGECHANGE,handlePageControlEvent);
			}
			
			tm=new TouchMove(this);
			tm.addEventListener(TouchMove.FLIPED,handlePageFliped);
			
			initPics();
		}
		
		protected function initPics():void
		{
			var kb:KBitmap;
			pics=new Vector.<KBitmap>(urls.length);
			for(var i:uint=0;i<urls.length;i++)
			{
				kb=new KBitmap();
				kb.url=urls[i];
				//kb.loadPic(urls[i]);
				this.addChild(kb);
				if(i==0)
				{
					kb.loadPic(urls[i]);
				}
				pics[i]=kb;
			}
			if(pageControl)
			{
				this.addChild(pageControl);
			}
			
		}
		
		protected function removeTm():void
		{
			if(tm)
			{
				tm.gc();
				tm.removeEventListener(TouchMove.FLIPED,handlePageFliped);
				tm=null;
			}
		}
		
		protected function removePic(kb:KBitmap):void
		{
			kb.clear();
		}
		
		protected function handlePageFliped(e:KEvent):void
		{
			
			changePageByDir(e.obj);
		}
		
		protected function handlePageControlEvent(e:KEvent):void
		{
			this.dispatchEvent(new KEvent("moved",null,true));
			if(!canChange)return;
			canChange=false;
			if(pageControl)
			{
				pageControl.mouseEnabled=false;
			}
			var targetNum:uint=uint(e.obj);
			var kb:KBitmap=pics[curNum];
			var targetPosition:Number;
			if(targetNum>curNum)
			{
				dir='left';
				targetPosition=-parentWidth;
			}
			else
			{
				dir='right';
				targetPosition=parentWidth;
			}
			lastKb=kb;
//			TweenLite.to(kb,0.5,{alpha:0,x:targetPosition,ease:Sine.easeOut,onComplete:removePic,onCompleteParams:[kb]});
			curNum=targetNum;
			showKb();
			
			if(pageControl)
			{
				pageControl.changePage(curNum);
			}
			this.dispatchEvent(new KEvent("pageChanged",curNum));
		}
		
		protected function showKb():void
		{
			var kb:KBitmap=pics[curNum];
			kb.addEventListener(KBitmap.LOADED,handleKbLoaded);
			kb.loadPic(kb.url);
			setTimeout(setCanChange,500);
		}
		
		protected function handleKbLoaded(e:KEvent):void
		{
			var kb:KBitmap=pics[curNum];
			kb.removeEventListener(KBitmap.LOADED,handleKbLoaded);
			trace(Time.hour_minute_second+":图片准备好了  "+kb.url+" 准备好了");
			var targetPosition:Number;
			if(dir=='left')
			{
				kb.x=parentWidth
			}
			else
			{
				kb.x=-parentWidth;
			}
			TweenLite.to(lastKb,0.5,{alpha:0,x:targetPosition,ease:Sine.easeIn,onComplete:removePic,onCompleteParams:[lastKb]});
			TweenLite.to(kb,0.5,{alpha:1,x:0,ease:Sine.easeOut});
		}
		
		public function changePageByDir(fx:String):void
		{
			this.dispatchEvent(new KEvent("moved",null,true));
			if(!canChange)return;
			canChange=false;
			if(pageControl)
			{
				pageControl.mouseEnabled=false;
			}
			dir=fx;
			var kb:KBitmap;
			kb=pics[curNum];
			var targetPosition:Number;
			if(dir=="left")
			{
				
				targetPosition=-parentWidth;
				curNum++;
				if(curNum>=pageTotal)
				{
					curNum=0;
				}
			}
			else
			{
				targetPosition=parentWidth;
				curNum--;
				if(curNum<0)
				{
					curNum=pageTotal-1;
				}
			}
			lastKb=kb;
			showKb();
			if(pageControl)
			{
				pageControl.changePage(curNum);
			}
			this.dispatchEvent(new KEvent("pageChanged",curNum));
		}
		
		public function nextPage():void
		{
			var kb:KBitmap;
			kb=pics[curNum];
			
			dir="left";
			var targetPosition:Number=-parentWidth;
			curNum++;
			if(curNum>=pageTotal)
			{
				curNum=0;
			}
			lastKb=kb;
			
			showKb();
			
			if(pageControl)
			{
				pageControl.changePage(curNum);
			}
			this.dispatchEvent(new KEvent("pageChanged",curNum));
		}
		
		
		protected function setCanChange():void
		{
			canChange=true;
			if(pageControl)
			{
				pageControl.mouseEnabled=true;
			}
		}
		
		protected function removePageControl():void
		{
			if(pageControl)
			{
				pageControl.gc();
				pageControl.removeEventListener(PageControl_IOS_style.PAGECHANGE,handlePageControlEvent);
				pageControl=null;
			}
		}
		
		
		protected function removePics():void
		{
			if(pics)
			{
				var kb:KBitmap;
				for(var i:uint=0;i<pics.length;i++)
				{
					kb=pics[i];
					kb.clear();
				}
				pics=null;
			}
		}
		
		public function gc():void
		{
			removeTm();
			removePageControl();
			removePics();
		}
		
	}
}
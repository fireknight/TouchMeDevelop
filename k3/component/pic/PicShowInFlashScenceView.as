package k3.component.pic
{
	/**
	 *	图片已经在flash里摆好了,图片是Sprite对象,按钮(影片对象,第二帧是被选择状态)和图片名称以 xxx_# 的形式命名(通过下划线获取后面的数字) ,并根据点击来显示图片,每次只显示一个图片对象.
	 */	
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import k3.event.KEvent;
	import k3.global.Time;
	public class PicShowInFlashScenceView extends Sprite
	{
		private var targetMc:Sprite;
		private var pics:Vector.<Sprite>;
		private var bts:Vector.<MovieClip>;
		private var lastPic:Sprite;
		private var lastBt:MovieClip;
		private var firstshow:Boolean;
		public function PicShowInFlashScenceView(mc:Sprite,_pics:Vector.<Sprite>,_bts:Vector.<MovieClip>,_scale:Number=1,_firstShow:Boolean=true)
		{
			this.addChild(mc);
			mc.scaleX=mc.scaleY=_scale;
			targetMc=mc;
			firstshow=_firstShow;
			pics=_pics;
			bts=_bts;
			if(firstshow)
			{
				lastBt=bts[0];
				lastBt.gotoAndStop(2);
			}
			
			init();
			
		}
		
		protected function init():void
		{
			var sp:Sprite;
			for(var i:uint=0;i<pics.length;i++)
			{
				sp=pics[i];
				sp.alpha=0;
				sp.mouseEnabled=false;
				if(i==0&& firstshow)
				{
					sp.alpha=1;
					lastPic=sp;
				}
			}
			var sb:MovieClip;
			for(i=0;i<bts.length;i++)
			{
				sb=bts[i];
				sb.addEventListener(MouseEvent.CLICK,handleBtClick);
			}
		}
		
		private function hidePics():void
		{
			var sp:Sprite;
			for(var i:uint=0;i<pics.length;i++)
			{
				sp=pics[i];
				sp.alpha=0;
			}
		}
		
		protected function handleBtClick(e:MouseEvent):void
		{
			var sb:MovieClip=MovieClip(e.currentTarget);
			trace(Time.hour_minute_second+": "+sb.name+" 点击了");
			if(lastBt)
			{
				if(lastBt==sb)return;
				lastBt.gotoAndStop(1);
			}
			hidePics();
			sb.gotoAndStop(2);
			var index:int=sb.name.indexOf("_");
			var target:uint=uint(sb.name.slice(index+1));
			lastPic=pics[target];
			lastBt=sb;
			TweenLite.to(lastPic,0.2,{alpha:1});
			targetMc.dispatchEvent(new KEvent("moved",null,true));
		}
		
		protected function removeBtEvent():void
		{
			if(bts)
			{
				var sb:MovieClip;
				for(var i:uint=0;i<bts.length;i++)
				{
					sb=bts[i];
					sb.removeEventListener(MouseEvent.CLICK,handleBtClick);
				}
				bts=null;
			}
			
		}
		
		public function gc():void
		{
			removeBtEvent();
		}
		
	}
}
package k3.component.container
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import k3.global.Time;
	import k3.event.KEvent;
	public class ContainerWithDragBar extends Sprite
	{
		protected var titleBar:Sprite;
		protected var conBg:Sprite;
		protected var closeBtn:Sprite;
		private var closed:Boolean=false;
		protected var con:Sprite;
		public function ContainerWithDragBar()
		{
			super();
		}
		
		public function init(_titleBar:Sprite,_conBg:Sprite,_con:Sprite,_clsBt:Sprite):void
		{
			titleBar=_titleBar;
			conBg=_conBg;
			closeBtn=_clsBt;
			con=_con;
			this.addChild(titleBar);
			titleBar.y=-titleBar.height;
			closeBtn.y=titleBar.y+(titleBar.height/2-closeBtn.height/2);
			closeBtn.x=titleBar.width-10-closeBtn.width;
			this.addChild(closeBtn);
			this.addChild(conBg);
			this.addChild(con);
			titleBar.addEventListener(MouseEvent.MOUSE_DOWN,handleTitleBarMouseDown);
			closeBtn.addEventListener(MouseEvent.CLICK,handleCloseBtClick);
		}
		
		protected function handleCloseBtClick(e:MouseEvent):void
		{
			this.dispatchEvent(new KEvent(KEvent.CLOSE,con));
		}
		
		protected function handleTitleBarMouseDown(e:MouseEvent):void
		{
			this.parent.addChild(this);
			titleBar.stage.addEventListener(MouseEvent.MOUSE_UP,handleMouseUp);
			titleBar.stage.addEventListener(MouseEvent.MOUSE_MOVE,handleMouseMove);
			this.startDrag(false);
		}
		
		protected function handleMouseMove(e:MouseEvent):void
		{
			if(closed)return;
			this.dispatchEvent(new KEvent("moved",null,true));
		}
		
		protected function handleMouseUp(e:MouseEvent):void
		{
			trace(Time.hour_minute_second+": 鼠标抬起");
			if(closed)return;
			this.stopDrag();
			titleBar.stage.addEventListener(MouseEvent.MOUSE_UP,handleMouseUp);
			titleBar.stage.addEventListener(MouseEvent.MOUSE_MOVE,handleMouseMove);
		}
		public function gc():void
		{
			closed=true;
			titleBar.removeEventListener(MouseEvent.MOUSE_DOWN,handleTitleBarMouseDown);
			closeBtn.removeEventListener(MouseEvent.CLICK,handleCloseBtClick);
		}
		
	}
}